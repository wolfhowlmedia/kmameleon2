/***************************************************************************
                          xmameconfig.h  -  description
                             -------------------
    copyright            : (C) 2002 by Whitehawk Stormchaser
    email                : zerokode@gmx.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef __KMMXMAMECONFIG_H_
#define __KMMXMAMECONFIG_H_

//KDE stuff...
#include <kdialogbase.h>
#include <klocale.h>
#include <kstandarddirs.h>
#include <kfiledialog.h>
#include <kmessagebox.h>
#include <kconfig.h>
#include <kapplication.h>
#include <kprocess.h>
#include <kcolorcombo.h>

//Qt stuff...
#include <qfile.h>
#include <qwidget.h>
#include <qlayout.h>
#include <qlabel.h>
#include <qcombobox.h>
#include <qcheckbox.h>
#include <qlineedit.h>
#include <qframe.h>
#include <qtoolbutton.h>
#include <qgroupbox.h>
#include <qslider.h>
#include <qframe.h>
#include <qspinbox.h>
#include <qtextbrowser.h>
#include <qdatetime.h>
#include <qpushbutton.h>

class XMameCfg : public KDialogBase
{
Q_OBJECT;
public:
	XMameCfg(QWidget *parent=0);
	~XMameCfg();

/* undefined */

	KConfig *config;
	QString loadedOptions;

#if KDE_VERSION >= 306
	KProcess adProc;
	KProcess nfoProc;
	KProcess sdlListmodesProc;
#else
	KShellProcess adProc;
	KShellProcess nfoProc;
	KShellProcess sdlListmodesProc;
#endif

	QStringList paths;
	bool loadGeneral;
	bool loadPaths;
	bool loadAudio;
	bool loadVideo;
	bool loadXGL;
	bool loadEffects;
	bool loadDevices;
	bool loadCustom;
	bool loadSdl;
	bool loadX11;
	bool loadThemeMgr;

/*general*/
	QLineEdit *pthXmameRoms;
	QLineEdit *pthXmameSamples;
	QLineEdit *pthXmameArt;
	QLineEdit *pthXmameSdlBin;
	QLineEdit *pthXmameX11Bin;
	QLineEdit *pthXmameGlBin;
	QLineEdit *pthXmameSvgaLibBin;
	QLineEdit *pthScreenShots;

	QCheckBox *chkOutput;
	QCheckBox *chkScreenShots;
	QCheckBox *chkRestoreWindowPos;
	QCheckBox *chkColorOutput;
	QCheckBox *chkCheatMode;
	QCheckBox *chkCustomSwitch;
	QCheckBox *chkShowListInstead;
	QLineEdit *leCheatFile;
	QToolButton *btnFindCheatFile;
	QCheckBox *chkDetachProcess;
	QComboBox *xMameType;
	QComboBox *cbConfigType;

/*audio*/
	QCheckBox *chkIsSound;
	QCheckBox *chkSamples;
	QCheckBox *chkFakeSound;

	QCheckBox *chkCustomAudioSf;
	QCheckBox *chkCustomAudioBufSize;
	QCheckBox *chkCustomAudioDevice;
	QCheckBox *chkCustomAudioMixerDevice;

	QLineEdit *audioSfEdit;
	QLineEdit *audioBufSizeEdit;
	QSlider *audioSetVolume;
	QLineEdit *audioDeviceEdit;
	QLineEdit *audioMixerDeviceEdit;
	QLabel *lblDisplayVolume;

/*video*/
	QComboBox *cbBpp;
	QComboBox *videoFx;

	QCheckBox *chkEnableFrameSkip;
	QCheckBox *chkFrameSkip;
	QSpinBox *spnMaxAutoFrameSkip;
	QSpinBox *spnNoAutoFrameSkip;

	QGroupBox* rotationBox;
	QCheckBox* chkFlipX;
	QCheckBox* chkFlipY;
	QComboBox* cbRotation;

/*GL configuration*/
	QLineEdit* glleTexSize;
	QLineEdit* glleDGlLib;
	QLineEdit* glleDGluLib;
	QLineEdit* glleVBeamSize;
	QLineEdit* gllecabModel;
	QLineEdit* glleXYRes;
	QCheckBox* glChkFullscreen;
	QCheckBox* glchkDblBuffer;
	QCheckBox* glchkBlit;
	QCheckBox* glchkGlExt;
	QCheckBox* glchkBilinear;
	QCheckBox* glchkBitmap;
	QCheckBox* glchkVBitmap;
	QCheckBox* glchkCMod;
	QCheckBox* glchkAb;
	QCheckBox* glchkAA;
	QCheckBox* glchkVAA;
	QCheckBox* glchkCabView;
	
/*SDL*/
	QComboBox *cbSdlFsModes;
	QCheckBox *chkSdlFullscreen;

/*X11*/
	QComboBox* cbX11WindowMode;
	QCheckBox* cbX11ForceYuv;
	QCheckBox* cbX11ForceYv12;
	QCheckBox* cbX11MitShm;
	QCheckBox* cbX11ShowCursor;
	QCheckBox* cbX11Widescreen;
	QCheckBox* cbX11XSync;

/*FX (effects)*/
	QCheckBox* chkScanlines;
	QCheckBox* chkArtwork;
	QCheckBox* chkBackdrops;
	QCheckBox* chkOverlay;
	QCheckBox* chkBezel;
	QCheckBox* chkArtCrop;
	QCheckBox* chkThrottle;
	QCheckBox* chkSleepIdle;
	QSpinBox* spnBrightness;
	QSpinBox* spnFrames;
	QSpinBox* spnXScale;
	QSpinBox* spnYScale;
	QSpinBox* spnScaleVideo;
	QSpinBox* spnArtResolution;
	QLineEdit* edtGamma;
	QLineEdit* edtAspectRatio;

/*devices*/
	QComboBox *inputBox;
	QCheckBox *chkAnalogStick;
	QLineEdit *customJoyDevName;
	QCheckBox *chkUseHotRod;
	QCheckBox *chkUseHotRodSe;
	QCheckBox *chkUsbPsPad;
	QCheckBox *chkRapidFire;
	QCheckBox *chkGrabMouse;

/*custom*/
	QTextEdit *customEditor;

/*info*/
	QTextBrowser* versionBrowser;
	
/*themes*/
	QLineEdit* leBgImage;

private slots:
//module loader...
	void loadModules();
	void lockLevelList();

//existing modules...
	void modAudio();
	void modGeneral();
	void modPaths();
	void modFirstRun();
	void modVideo();
	void modGl();
	void modX11();
	void modSdl();
	void modDevices();
	void modEffects();
	void modInfo();
	void modCustom();
	void modThemes();

//buttons
	void slotSetSdlMameBin();
	void slotSetX11MameBin();
	void slotSetGlMameBin();
	void slotSetSvgaLibMameBin();
	void slotSetXMameRomDir();
	void slotSetXMameSamplesDir();
	void slotSetXMameArtDir();
	void slotSetCheatFile();
	void loadCurrentParameters();
	void slotGetSdlFsModes();
	void slotSetSSDir();

//everything else
	void setDefaultPaths( KProcess *, char *, int);
	void setVersionInfo( KProcess *, char *, int);
	void loadSdlFsModes( KProcess *, char *, int);
	void changeVolumeLevel(int);
	void slotConfigType(int);
	void reload();
	void modInfoReload();
	void xMameTypeChanged(int);

protected slots:
	void slotOk();
	int xmModes(int);

public slots:
	void loadConfig();
	void writeConfig();
	void saveOptions(bool);

signals:
	void listenConfOk();
};

#endif
