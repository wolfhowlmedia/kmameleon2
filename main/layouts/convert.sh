#!/bin/sh
# Copyright (2003) Whitehawk Stormchaser
#
# Script to convert .ui files to .h/.cc pair
# some parts of the file, like the sed line and temp file
# generator Copyright (c) the KDE team

# configure these as needed:
qt_path="/usr/lib/qt3"     #Both must be set up
kde_path="/opt/kde3"          #to make this thing work
i18n_switch="-tr i18n"
# end configure

# don't touch from here on, if you don't know what you are doing:
shopt -s extglob; #shopt -s foo sets the foo shell option.
file_version="Version 0.2"; #version of the file
case $1 in
	clean)
		echo rm -f *.cc *.h *.moc
		rm -f *.cc *.h *.moc
		exit 0;
	;;
	boneclean)
		echo "Cleaning up";
		echo rm -f !(*.ui|*.sh)
		rm -f !(*.ui|*.sh);
		exit 0;
	;;
	all)
		#just need this option 
	;;
	file)
		compile=$2
		if test -z $2; then
			echo "Fatal: You need to specify a valid filename!";
			exit 127;
		fi
		if test -s $2; then
			echo;
		else
			echo "Fatal: You need to specify a valid filename!";
			exit 127;
		fi
	;;
	version)
		echo $file_version;
		exit 0;
	;;
	*)
		echo "Usage: convert.sh [clean|boneclean|all|file <filename>]]";
		echo "clean           |Cleans all the files produced in compilation";
		echo "boneclean       |Cleans the directory, leaving only *.sh,*.ui and hidden files";
		echo "all             |Compiles every .ui file in directory";
		echo "file <filename> |Compiles a specific .ui file";
		echo "version         |Outputs version and exits";
		exit 0;
	;;
esac


if test -n "$qt_path" && test -n "$kde_path" && test -d "$qt_path" && test -d "$kde_path"; then
	use_uic_path="$qt_path/bin/uic"
	use_moc_path="$qt_path/bin/moc"
	designer_plugin_switch="-L $kde_path/lib/kde3/plugins/designer"
else
	echo "Fatal: You have to set up the program first..."
	echo "Set the qt_path in the script to your current \$QTDIR"
	echo "Set the kde_path in the script to your current \$KDEDIR"
	echo;
	echo "Current setup:"
	echo "   qt_path=$qt_path"
	echo "   kde_path=$kde_path"

	exit 127
fi

_compile ()
{
	chopped_file="${current_ui_file%%.*}"
	
	echo uic: -o $chopped_file.h $designer_plugin_switch -nounload $current_ui_file
	$use_uic_path "-o"$chopped_file".h" $designer_plugin_switch -nounload ./$current_ui_file #create header
	
	echo moc: $chopped_file.h -o $chopped_file.moc
	$use_moc_path $chopped_file".h" -o $chopped_file".moc" #create moc
	
	echo "echo '#include <klocale.h>' > $chopped_file.cc"
	echo '#include <klocale.h>' > $chopped_file.cc #create dummy .cc file with klocale header
	
	echo uic: -i $chopped_file.h $i18n_switch $designer_plugin_switch $current_ui_file -o $chopped_file.cc.temp
	$use_uic_path "-i" $chopped_file".h" $i18n_switch $designer_plugin_switch ./$current_ui_file -o $chopped_file".cc.temp"; ret=$?;

	sed -e "s,tr2i18n( \"\" ),QString::null,g" $chopped_file".cc.temp" | sed -e "s,tr2i18n( \"\"\, \"\" ),QString::null,g" | sed -e "s,image\([0-9][0-9]*\)_data,img\1_test,g" >> $chopped_file".cc"
	
	echo rm -f "$chopped_file.cc.temp";
	rm -f "$chopped_file.cc.temp";
	
	if test "$ret" = 0; then echo '#include "'$chopped_file'.moc"' >> "$chopped_file.cc"; else rm -f "$chopped_file.cc" ; exit $ret ; fi
}

if test -x $use_uic_path; then
	echo "QT:"
		echo -n "uic: "; $use_uic_path -version
		echo -n "moc: "; $use_moc_path -v
	echo;
	echo "KDE:";
		$kde_path"/bin/kde-config" --version
	echo;
	
	echo "Compiling: "
	
	case $1 in
	all)
		for current_ui_file in *.ui
		do
			_compile
		done
	;;
	file)
		current_ui_file=$2;
		_compile
	;;
	esac
else
	echo "Fatal: Can't find uic compiler..."
	exit 127
fi

