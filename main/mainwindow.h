/***************************************************************************
                          mainwindow.h  -  description
                             -------------------
    copyright            : (C) 2002 by Whitehawk Stormchaser
    email                : zerokode@gmx.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _KMAMELEON_H__
#define _KMAMELEON_H__

//Qt stuff...
#include <qwidget.h>
#include <qtoolbutton.h>
#include <qpushbutton.h>
#include <qpixmap.h>
#include <qtooltip.h>
#include <qcursor.h>
#include <qfileinfo.h>
#include <qstringlist.h>
#include <qtextstream.h>
#include <qfile.h>

//KDE stuff...
#include <kapplication.h>
#include <kuniqueapplication.h>
#include <klocale.h>
#include <khelpmenu.h>
#include <kstandarddirs.h>
#include <kpopupmenu.h>
#include <kprocess.h>
#include <kconfig.h>
#include <kdebug.h>
#include <kdeversion.h>

//custom...
#include "kmmversion.h"
#include "dlgkmmiface.h"
#include "kmmdebug.h"
#include "xmameconfig.h"
#include "kmmscreenshots.h"
#include "kmmgameslist.h"

//Tools...
#include "toolromchecker.h"
#include "toolpathfinder.h"

class KMameleon : public mainDoor
{
Q_OBJECT
public:
	KMameleon();
	~KMameleon();

	KHelpMenu *helpMenu;
	KConfig *config;
	KPopupMenu *mnuPopup;
	KPopupMenu *toolPopup;

	QString conf_selectedRomPath;
	QString conf_selectedGame;
	QString conf_xmameBinPath;
	QString setLibrary;
	QString fsOptions;

	ScreenShotWindow *ssWindow;
	DevDebug *dbgWindow;
	XMameCfg *newConfig;
	ItemGamesList *glWindow;

//Tools-only definitions...
	ToolRomCheckerPlugin *romChecker;
	ToolPathFinderPlugin *pathFinder;
private:
//have to check for the right versions... KProcess can't use setUseShell(bool)
#if KDE_VERSION >= 306
	KProcess proc;
#else
	KShellProcess proc;
#endif

	QString temp;
	QString outBuff;
	QString options;
	void saveWindowsPos();


private slots:
	void runPlay();
	void goStop();
	void runConf();
	void reload();
	void reloadList();
	void popHelp();
	void popTools();
	void getOutput( KProcess *, char *, int);
	void getErrOutput( KProcess *, char *, int);
	void getStoppedStatus(KProcess *);
	QString modes(int);
	void loadPopups();
	void loadScreenShots();

//tools...:
	void loadRomChecker();
	void loadPathFinder();

protected slots:
	void closeEvent (QCloseEvent *);

protected:
	void keyPressEvent( QKeyEvent * );	
};

#endif
