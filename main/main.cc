/***************************************************************************
                          main.cpp  -  description
                             -------------------
    begin                : Sun Jun 23 2002 23.58
    copyright            : (C) 2002 by Whitehawk Stormchaser
    email                : zerokode@gmx.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <kcmdlineargs.h>
#include <kaboutdata.h>
#include <klocale.h>
#include <stdlib.h>

#include "kmmversion.h"
#include "mainwindow.h"

static const char *description =
	I18N_NOOP("X Multiple Arcade Machine Emulator wrapper");

static KCmdLineOptions options[] =
{
  { 0, 0, 0 }
};

int main(int argc, char *argv[])
{

  KAboutData aboutData( "kmameleon", I18N_NOOP("KMameleon"),
    __kmmversion__, description, KAboutData::License_GPL,
    "(c) 2002-2007, Primoz Anzur", 0, "http://kmameleon.berlios.de");
  aboutData.addAuthor("Primoz Anzur",0, "zerokode@gmx.net");
  aboutData.addCredit("Pacome MASSOL","Wrote an XML parser for gamelist", "pacome@pmassol.net");
  aboutData.addCredit("Richard Moore","coding help", "rich@kde.org");
  aboutData.addCredit("Dirk Mueller","Fix some compile bugs", "mueller@kde.org");
  aboutData.addCredit("Waldo Bastian","Explained about plugins", "bastian@kde.org");
  KCmdLineArgs::init( argc, argv, &aboutData );
  KCmdLineArgs::addCmdLineOptions( options );
  KUniqueApplication::addCmdLineOptions();

  if (!KUniqueApplication::start()) {
       fprintf(stderr, "KMameleon is already running!\n");
       exit(0);
  }
  KUniqueApplication a;
  KMameleon *mameleon = new KMameleon();
  a.setMainWidget(mameleon);
  mameleon->show();

  return a.exec();
}
