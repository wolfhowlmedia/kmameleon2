/***************************************************************************
                          kmmgameslist.cc  -  description
                             -------------------
    copyright            : (C) 2002 by Whitehawk Stormchaser
    email                : zerokode@gmx.net
    
    Last modified        : 2005-03-25 by Pacome MASSOL (pacome@pmassol.net)
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#include "kmmgameslist.h"
#include <qregexp.h>
#include "kdebug.h"

ItemGamesList::ItemGamesList( QWidget *parent, const char *name, WFlags f ) : GameList( parent, name, f ), QXmlDefaultHandler (){
        // catch some signals
	connect (&infoProc, SIGNAL( receivedStdout (int , int &)), this, SLOT( getCheck( int, int &)) );
	connect (lvGameList, SIGNAL( selectionChanged( QListViewItem* ) ), this, SLOT( selChanged( QListViewItem* ) ) );
	connect (lvGameList, SIGNAL(doubleClicked( QListViewItem *, const QPoint &, int )), this, SLOT(runPlay( )) );
	connect (lvGameList, SIGNAL(returnPressed( QListViewItem *, const QPoint &, int )), this, SLOT(runPlay( )) );
	
	connect (btPresents, SIGNAL( stateChanged( int ) ), this, SLOT( refreshPresents( int ) ) );
        connect (btSupported, SIGNAL( stateChanged( int ) ), this, SLOT( refreshSupported( int ) ) );
	
	//Qt adds some weird numbers to QListView, if header string is QString::null
	lvGameList->header()->setLabel( 0, "" );

	//first things first...
	config = kapp->config();
}

ItemGamesList::~ItemGamesList() {
	
}

QStringList ItemGamesList::getPresent(void) {
	return gamesPresent;
}

void ItemGamesList::buildLists(bool refresh) {
	kdDebug() << "buildLists ()" << endl;
	config -> setGroup("general");
	bool found=false;
	buildVerifiedRomList();

        pixSnap->setPixmap(QPixmap(locate("data","kmameleon/pics/noss.png")));


        //for now; we'll load path from the config fle..
	if(!refresh) {
		// fill games lists
		fillSupported();
		fillPresent();
		if (config->readBoolEntry("displayType")) {
			btPresents->setChecked(true);
		}
	} else {
		reload();
	}
        // Select last game played
        QString lastPlayed = config -> readEntry("last_played","");
	
        QListViewItemIterator it( lvGameList );
        while ( (item = it.current()) && !found) {
                if(item->text(1)==lastPlayed) {
                        lvGameList->setSelected(item, true);
                        lvGameList->ensureItemVisible(item);
                        found=true;
                }
                ++it;
        }
	verifyRoms ();
}


void ItemGamesList::fillPresent(void) {
	// presents list loaded from files in the rom path
	config->setGroup("general");
	QString mameRomPath = config->readEntry("xmame_rom","");

	if (! mameRomPath.isEmpty())
	{
		gamesPresent.clear();

		QDir d(mameRomPath);
		d.setFilter( QDir::Dirs);

		for ( unsigned int i=0; i<d.count(); i++ )
		{
			if ((d[i] != ".") && (d[i] != ".."))
			{
				if (gamesPresent.findIndex(d[i].stripWhiteSpace()) == -1)
				{
					gamesPresent += d[i];
					//kdDebug() << "Adding game name [directory]: " << d[i] << endl;
				}
			}
		}

		d.setFilter( QDir::Files);
		QStringList zipGamesList = d.entryList("*.zip");
		for (unsigned int i=0; i<zipGamesList.count(); i++)
		{
			QFileInfo fi(zipGamesList[i]);
			if (gamesPresent.findIndex(fi.baseName()) == -1)
			{
				//kdDebug() << "Adding game name [zip]: " << fi.baseName() << endl;
				gamesPresent += fi.baseName();
			}
			else {
				kdDebug() << "Ignoring game name [zip]: " << fi.baseName() << endl;
			}
		}

		QStringList bZipGamesList = d.entryList("*.bz2");
		for (unsigned int i=0; i<bZipGamesList.count(); i++)
		{
			QFileInfo fi(bZipGamesList[i]);
			if (gamesPresent.findIndex(fi.baseName()) == -1)
			{
				//kdDebug() << "Adding game name [bZip2]: " << fi.baseName() << endl;
				gamesPresent += fi.baseName();
			}
			else {
				kdDebug() << "Ignoring game name [bZip2]: " << fi.baseName() << endl;
			}
		}

		kdDebug() << "Sorting list..." << endl;
		gamesPresent.sort();
		presents=gamesPresent.count();
		lcdPresents->display(presents);
	} else {
		kdDebug() << "Rom path not configured !" << endl;
	}
}

void ItemGamesList::fillSupported(void) {
	// Supported list loaded from :
	// - an xml parsing of xmame -lx output. Then, a file called gamelist.xml is generated in the home directory
	// - gamelist.xml if exists

	QString fileName = locate("appdata","gamelist.xml");
	int num=-1;
	
	// check if gamelist.xml exists
	if(!fileName.isNull()) {
		splash->message( i18n("Nice! Game list exists!") , Qt::AlignBottom);
		QFile fic(fileName);
		fic.open(IO_ReadOnly);
		getCheck(fic.handle(), num);
	}
	else 
	{  // No ! so create it with xmame -lx
		splash->message( i18n("Populating game list...") , Qt::AlignBottom);
		//OK... NEEDED: Dynamical detection of xmame :)
		if (! modes(0).isEmpty()) {
			conf_xmameBinPath = modes(0);
		} else if (! modes(1).isEmpty()) {
			conf_xmameBinPath = modes(1);
		} else if (! modes(2).isEmpty()) {
			conf_xmameBinPath = modes(2);
		} else if (! modes(3).isEmpty()) {
			conf_xmameBinPath = modes(3);
		} else {
			return;
		}
		
		config->setGroup("general");
		QString mameRomPath = config->readEntry("xmame_rom","");
		QString options ="";
		options += " -lx ";
		
		infoProc.clearArguments();
		#if KDE_VERSION >= 306
			infoProc.setUseShell(true);
		#endif
		
		infoProc << conf_xmameBinPath << " " << options.local8Bit() ;
		kdDebug() << conf_xmameBinPath << " " << options.local8Bit() << endl;
		if (! infoProc.start(KProcess::Block, (KProcess::Communication)(KProcess::NoRead|KProcess::Stdout))) {
			kdDebug() <<"Can't exec the process..." << endl;
		} else {
			infoProc.resume();
		}
		
		// generate gamelist.xml
		saveSupported();
	}
}

void ItemGamesList::saveSupported(void) {
	// create a file
	xmlFile.setName(locateLocal("appdata","gamelist.xml", false));
	
	if ( !xmlFile.open( IO_WriteOnly ) ) {
		kdDebug() << "Unable to create gamelist.xml" << endl;
		return;
	}

	QTextStream txtStream( &xmlFile );

	txtStream << "<?xml version=\"1.0\"?>" << endl;
	txtStream << "<kmameleon>" << endl;
	
	QListViewItemIterator it( lvGameList );
	while ( item = it.current() ) {
		txtStream << "<game name = \"" << removeChar(item->text(1)) << "\"" ;
		if(item->text(1)!=item->text(2)) {
			txtStream << " cloneof = \"" << removeChar(item->text(2)) << "\" >" << endl;
		} else {
			txtStream << ">" << endl;
		}
		txtStream << "\t<year>" << removeChar(item->text(3)) << "</year>" << endl;
		
		txtStream << "\t<manufacturer>" << removeChar(item->text(4)) << "</manufacturer>" << endl;
		
		txtStream << "\t<description>" << removeChar(item->text(5)) << "</description>" << endl;
		txtStream << "</game>" << endl;
		++it;
	}
	txtStream << "</kmameleon>" << endl;
	xmlFile.close();
}

QString ItemGamesList::removeChar(QString strTxt) {
	// transform some chars in entities : <, >, &

QString strTemp;
	
	strTemp = strTxt.replace(QChar('<'), "&lt;");
	strTemp = strTemp.replace(QChar('>'), "&gt;");
	strTemp = strTemp.replace(QChar('&'), "&amp;");

	return strTemp;
}

QString ItemGamesList::removeEntities(QString strTxt) {
	// transform entities in chars

QString strTemp;
	
	strTemp = strTxt.replace("&lt;", QChar('<'));
	strTemp = strTemp.replace("&gt;", QChar('>'));
	strTemp = strTemp.replace("&amp;", QChar('&'));

	return strTemp;
}

QListViewItem * ItemGamesList::getSelect(void){
	return lvGameList->selectedItem();
}

void ItemGamesList::closeEvent(QCloseEvent *) {
	QKeyEvent me( QEvent::KeyPress, Qt::Key_F12, 0, 0);
	QApplication::sendEvent( (QObject *)parentWidget(), &me );
}

void ItemGamesList::selChanged( QListViewItem* newSel ) {
// if selection changed, changes the snapshot

	config->setGroup("general");
	QString mameSnapPath = config->readEntry("xmame_screenshots","");
	QPixmap pix = QPixmap( mameSnapPath + "/" + newSel->text(2) + ".png" );
	if (!pix.isNull()) {
		pixSnap->setPixmap(pix);
	} else {
		// then try jpg
		pix = QPixmap( mameSnapPath + "/" + newSel->text(2) + ".jpg" );
		if (!pix.isNull()) {
			pixSnap->setPixmap(pix);
		} else {
			pixSnap->setPixmap(QPixmap(locate("data","kmameleon/pics/noss.png")));
		}
	}
}

void ItemGamesList::refreshPresents( int test) {
	kdDebug() << "refreshPresents: " << test << endl;
	kdDebug() << "there" << endl;
// updates gamelist with presents
	QListViewItemIterator it( lvGameList );
	int nt=0;
	
	while ( item = it.current() ) 
	{
		kdDebug() << item->text(1) << endl;
		nt++;
		++it;
		if(gamesPresent.find(item->text(1)) == gamesPresent.end()) {
			delete item;
		}
	}
	
	// save user config
	config->setGroup("general");	
	config->writeEntry("displayType", true);
	config->sync();
	kdDebug() << "void ItemGamesList::refreshPresents( int )" << endl;
	if (test == 2) {
		verifyRoms ();
	}
}

void ItemGamesList::refreshSupported( int test) {
	// updates gamelist with supported
	kdDebug() << "here" << endl;
	lvGameList->clear();

	QString fileName = locate("appdata","gamelist.xml");
	int num=-1;
	
	// check if gamelist.xml exists
	if(!fileName.isNull()) 
	{
		QFile fic(fileName);
		fic.open(IO_ReadOnly);
		getCheck(fic.handle(), num);
	}
	lvGameList->setSelected(lvGameList->firstChild(), true);
	
	// save user config
	config->setGroup("general");	
	config->writeEntry("displayType",false);
	config->sync();
	kdDebug() << "void ItemGamesList::refreshSupported( int )" << endl;
	if (test == 2) {
		verifyRoms ();
	}
}

void ItemGamesList::reload ( void ) {

	kdDebug() << "reload()" << endl;
	
	lvGameList->clear();

	// delete gamelist.xml
	QString fileName = locate("appdata","gamelist.xml");
	
	QDir d("");
	d.remove (fileName, true);
	
	// rescan
	splash->show();
	splash->QWidget::setCursor( QCursor(Qt::WaitCursor) );
	fillSupported();
	fillPresent();
	verifyRoms ();
	btSupported->setChecked(true);
	splash->QWidget::setCursor( QCursor(Qt::ArrowCursor) );
	splash->hide();
}

QString ItemGamesList::modes(int mode) {
//we just need to detect one binary, that it will check the roms...
	config->setGroup("general");
	if (mode == 0) {
		conf_xmameBinPath=config->readEntry("xmame_sdl","");
	} else if(mode == 1) {
		conf_xmameBinPath=config->readEntry("xmame_x11","");
	} else if(mode == 2) {
		conf_xmameBinPath=config->readEntry("xmame_gl","");
	} else if(mode == 3) {
		conf_xmameBinPath=config->readEntry("xmame_svga","");
	}
	return conf_xmameBinPath;
}

//***************************
// functions used to parse xml output
//****************************
void ItemGamesList::getCheck(int fd, int &)
{
	kdDebug() << "Start of parsing" << endl;
	
	supported=0;
	
	if(!xmlFile.open(IO_ReadOnly, fd))
	{
		kdDebug() << "Can't open xml source" << endl;
		exit(0);
	}
	
	QXmlInputSource source( &xmlFile );
	
	QXmlSimpleReader reader;
	QXmlContentHandler *handler =  this;

	reader.setContentHandler(handler );
	
	if(!reader.parse( source )) {
		kdDebug() << "Parse failed" << endl;
	}
}


bool ItemGamesList::startElement( const QString& , const QString& , const QString& qName, const QXmlAttributes& attributes) 
{
	bool runnable=true;
	element=(QString)qName;

	if (element == "game") {
		if ( attributes.length() > 0 ) {
			//runnable ?
			for ( int i = 0 ; i < attributes.length(); i++ ) 
			{
				if (attributes.qName(i) == "runnable" && attributes.value(i) == "no") { 		runnable=false;
				}
			}
			
			if (runnable) {

				supported++;
				item = new QListViewItem( lvGameList );
				for ( int i = 0 ; i < attributes.length(); i++ ) {   
					//runnable ?
					if (attributes.qName(i) == "name") {
						item->setText(1,removeEntities(attributes.value(i)));
						item->setText(2,removeEntities(attributes.value(i)));
					}
					if (attributes.qName(i) == "cloneof") {
						item->setText(2,removeEntities(attributes.value(i)));
					}
				//kdDebug() << attributes.qName(i) << attributes.value(i) << endl;
			}
		}
        }
    }
    return TRUE;
}

bool ItemGamesList::characters ( const QString & ch ) {
	if (element == "year" && !ch.stripWhiteSpace().isEmpty()) {
		item->setText(3,removeEntities(ch));
	}
	if (element == "manufacturer" && !ch.stripWhiteSpace().isEmpty()) {
		item->setText(4,removeEntities(ch));
	}
	if (element == "description" && !ch.stripWhiteSpace().isEmpty()) {
		item->setText(5,removeEntities(ch));
	}
	return TRUE;
}

bool ItemGamesList::endElement( const QString&, const QString&, const QString& ) {
    //item = item->itemBelow();
    return TRUE;
}

bool ItemGamesList::endDocument(void) {
    kdDebug() << "End of parsing." << endl;
    kdDebug() << "Supported games :" << supported << endl;
    infoProc.closeStdout();
    infoProc.kill();
    lcdSupported->display(supported);
    xmlFile.close();
    return TRUE;
}

void ItemGamesList::runPlay( ){
	QKeyEvent me( QEvent::KeyPress, Qt::Key_Return, 0, 0);
	QApplication::sendEvent( (QObject *)parentWidget(), &me );
}

void ItemGamesList::runConf( ){
	QKeyEvent me( QEvent::KeyPress, Qt::Key_F11, 0, 0);
	QApplication::sendEvent( (QObject *)parentWidget(), &me );
}

void ItemGamesList::verifyRoms( ) {
	config->setGroup("general");
	QString conf_xmameBinPath;
	QMap<QString, QString> vfMap;
	
	QString vFileName (locateLocal("appdata", "verified.roms"));

	if(!vFileName.isNull()) 
	{
		QFile vF(vFileName);
		vF.open(IO_ReadOnly);
		QTextStream data(&vF);
		QStringList sl(QStringList::split(QString("\n"), data.read().stripWhiteSpace()));
		kdDebug() << "verified roms: " << sl.count() << endl;
		for (unsigned int x = 0; x < sl.count(); x++) 
		{
			QStringList romlist(QStringList::split(QRegExp("[\\s+]"), sl[x].stripWhiteSpace()));
			if (romlist.count() >= 2) 
			{
				if (romlist[0].stripWhiteSpace() == "romset") { //hack for the new vresions of xmame...
					vfMap.insert(romlist[1].stripWhiteSpace(), romlist[2].stripWhiteSpace() + " " + romlist[3].stripWhiteSpace());
				} else {
					vfMap.insert(romlist[0].stripWhiteSpace(), romlist[1].stripWhiteSpace()  + " " + romlist[2].stripWhiteSpace());
				}
			}
		}
		vF.close();
	}
	else
	{
		return;
	}

        QListViewItemIterator it( lvGameList );
        while ( (item = it.current())) 
        {
		if (vfMap[item->text(1).stripWhiteSpace()].stripWhiteSpace() == "incorrect") {
			item->setPixmap(0, locate("data", "kmameleon/pics/rom_bad.png"));
		} else if (vfMap[item->text(1).stripWhiteSpace()].stripWhiteSpace() == "not found") {
			item->setPixmap(0, locate("data", "kmameleon/pics/rom_none.png"));
		} else {
			item->setPixmap(0, locate("data", "kmameleon/pics/rom_ok.png"));
		}
		++it;
	}
}

void ItemGamesList::buildVerifiedRomList( void )
{
	kdDebug() << "buildVerifiedRomList( void )" << endl;
	config->setGroup("general");
	QString vFileName (locateLocal("appdata", "verified.roms"));
		
	if (! modes(0).isEmpty()) {
		conf_xmameBinPath = modes(0);
	} else if (! modes(1).isEmpty()) {
		conf_xmameBinPath = modes(1);
	} else if (! modes(2).isEmpty()) {
		conf_xmameBinPath = modes(2);
	} else if (! modes(3).isEmpty()) {
		conf_xmameBinPath = modes(3);
	} else {
		return;
	}
	
	QString mameRomPath = config->readEntry("xmame_rom","");
	kdDebug() << mameRomPath << endl;
	QString options = conf_xmameBinPath + " -rp " + mameRomPath + " -vr > " + vFileName;
	fproc.clearArguments();
	#if KDE_VERSION >= 306
		fproc.setUseShell(true);
	#endif
	fproc <<  " " << options.local8Bit() ;
	
	kdDebug() << options << endl;
	
	if (!fproc.start(KProcess::NotifyOnExit, (KProcess::Communication)(KProcess::NoRead|KProcess::Stdout))) {
		kdDebug() << "Can't execute process..." << endl;
	} else {
		fproc.resume();
	}
	fproc.closeStdout();
	fproc.kill();
}


#include "kmmgameslist.moc"
