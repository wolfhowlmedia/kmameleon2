/***************************************************************************
                          kmmgameslist.h  -  description
                             -------------------
    copyright            : (C) 2005 by Whitehawk Stormchaser
    email                : zerokode@gmx.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef _KMMGAMESLIST_H__
#define _KMMGAMESLIST_H__

//KDE
#include <kdebug.h>
#include <kconfig.h>
#include <kapplication.h>
#include <klocale.h>
#include <kstandarddirs.h>
#include <kprocess.h>
#include <kmessagebox.h>
#include <kdeversion.h>


//QT
#include <qpushbutton.h>
#include <qtoolbutton.h>
#include <qlistview.h>
#include <qtooltip.h>
#include <qfileinfo.h>
#include <qdir.h>
#include <qstringlist.h>
#include <qlcdnumber.h>
#include <qlabel.h>
#include <qtextedit.h>
#include <qxml.h>
#include <qcursor.h>
#include <qsplashscreen.h>
#include <qradiobutton.h>
#include <qevent.h>
#include <qheader.h>
#include <qmap.h>


//Custom...
#include "dlggamelist.h"

class ItemGamesList : public GameList, private QXmlDefaultHandler
{
Q_OBJECT
public:
	ItemGamesList( QWidget *parent, const char *name, WFlags f );
	~ItemGamesList();
	void buildLists(bool);
	QListViewItem *getSelect(void);
	QSplashScreen *splash;
	QStringList getPresent(void);

private:
//have to check for the right versions... KProcess can't use setUseShell(bool)

#if KDE_VERSION >= 306
	KProcess infoProc;
	KProcess vProc;
	KProcess fproc;
#else
	KShellProcess vProc;
	KShellProcess infoProc;
	KShellProcess fproc;
#endif
	KConfig *config;
	QString conf_xmameBinPath;
	QListViewItem *item;

	QString gametocheck;
	int supported;
	int presents;
	void fillSupported(void);
	void fillPresent(void);
	QStringList gamesPresent;
	QString modes(int);
	void saveSupported(void);
	
	// xml parser
	QFile xmlFile;
	QString element;
	
	bool startElement( const QString&, const QString&, const QString& qName, const QXmlAttributes& );
	bool characters ( const QString & );
	bool endElement( const QString&, const QString&, const QString& );
	bool endDocument(void);
	QString removeChar(QString);
	QString removeEntities(QString);
	
protected slots:
	void closeEvent(QCloseEvent *);
	void getCheck(int , int &);
	void selChanged( QListViewItem* );
	void refreshPresents( int );
	void refreshSupported( int );
	void reload( void );
	void runPlay( void );
	void runConf( void );
	void verifyRoms ( void );
	void buildVerifiedRomList ( void );
};

#endif
