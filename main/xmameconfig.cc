/***************************************************************************
                          xmameconfig.cc  -  description
                             -------------------
    copyright            : (C) 2002 by Whitehawk Stormchaser
    email                : zerokode@gmx.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#include "xmameconfig.h"
#include <kdebug.h>
#include <qtooltip.h>

XMameCfg::XMameCfg(QWidget *parent) : KDialogBase(IconList, i18n("Preferences"), Ok|Cancel, Ok, parent, 0, false)
{

//set modules to inactive...
	loadGeneral = false;
	loadPaths = false;
	loadAudio = false;
	loadVideo = false;
	loadX11 = false;
	loadSdl = false;
	loadXGL = false;
	loadEffects = false;
	loadDevices = false;
	loadCustom = false;
	loadThemeMgr = false;
	
//general connections...
	connect(&adProc, SIGNAL( receivedStdout(KProcess *, char *, int)), this, SLOT( setDefaultPaths( KProcess *, char *, int)) );
	connect(&nfoProc, SIGNAL( receivedStdout(KProcess *, char *, int)), this, SLOT( setVersionInfo( KProcess *, char *, int)) );
	connect(&sdlListmodesProc, SIGNAL( receivedStdout(KProcess *, char *, int)), this, SLOT( loadSdlFsModes( KProcess *, char *, int)) );

	config = kapp->config();
	resize(300,400);
	loadModules();
}

XMameCfg::~XMameCfg()
{
}

void XMameCfg::loadModules()
{

//here is the list of loaded modules...
	modGeneral();
	modPaths();
	modAudio();
	modVideo();
	modX11();
	modSdl();
	modGl();
	modEffects();
	modDevices();
	modCustom();
	modInfo();
//	modThemes();

	int mode;
	for (int x = 0; x < 4; x++) {
		if (xmModes(x) > 0) {
			mode = xmModes(x);
		}
	}


//we won't need the module for first time more than JUST a first time...
	config->setGroup("First_run");
	if(config->readBoolEntry("First",true)) {
		modFirstRun();
		//first run...
		config->setGroup("First_run");
		config->writeEntry("First",false);
		config->sync();
	}
	else if (mode == 0)
	{
		loadConfig();
		reload();
	}
	else {
		loadConfig();
	}
}

void XMameCfg::modInfo()
{
	QFrame *page1 = addPage( i18n("Info"), i18n("Information"), locate("data","kmameleon/pics/mod_info.png") );

	QLabel* textLabel1;
	QGridLayout* Form1Layout;
	QGridLayout* layout5;

	Form1Layout = new QGridLayout( page1, 1, 1, 5, 5, "Form1Layout");

	layout5 = new QGridLayout( 0, 1, 1, 0, 4, "layout5");

	versionBrowser = new QTextBrowser( page1, "versionBrowser" );
	versionBrowser->setTextFormat( QTextBrowser::PlainText );

	layout5->addWidget( versionBrowser, 1, 0 );
	textLabel1 = new QLabel( page1, "textLabel1" );
	textLabel1 -> setText (i18n("XMame version information:"));
	layout5->addWidget( textLabel1, 0, 0 );
	Form1Layout->addLayout( layout5, 0, 0 );
	
	//modInfoReload();
}

void XMameCfg::xMameTypeChanged(int)
{
	modInfoReload();
}

void XMameCfg::modInfoReload()
{
	
	versionBrowser -> clear();
	QStringList xMameBinList;

	if (! pthXmameSdlBin -> text().isEmpty())
		xMameBinList += pthXmameSdlBin->text();
	if (! pthXmameX11Bin -> text().isEmpty())
		xMameBinList += pthXmameX11Bin -> text();
	if (! pthXmameGlBin -> text().isEmpty())
		xMameBinList += pthXmameGlBin -> text();
	if (! pthXmameSvgaLibBin -> text().isEmpty())
		xMameBinList += pthXmameSvgaLibBin -> text();

	versionBrowser -> insert(i18n("Executables found: %1\n\n").arg(xMameBinList.count()));
	versionBrowser -> insert(i18n("Locations:\n"));
	for (unsigned int xx = 0; xx < xMameBinList.size(); xx++)
	{
		if (! xMameBinList[xx].isEmpty())
			versionBrowser -> insert(xMameBinList[xx] + "\n");
	}

	kdDebug() << "Executables count: " << xMameBinList.count() << endl;
	
	if (xMameType -> currentItem() == 0)
	{
		QString options;
		options = xMameBinList[0];
		options +=" --version ";

		nfoProc.clearArguments();
		#if KDE_VERSION >= 306
			nfoProc.setUseShell(true);
		#endif

		nfoProc << options.local8Bit();
		if (! nfoProc.start(KProcess::Block, (KProcess::Communication)(KProcess::Stdout)) )
			kdDebug() << "Cannot execute the version query for XMame; Options called:\n"<< options << endl;
	}
	else
	{
		config->setGroup("general");
		
		QString xMameBin;
		switch (xMameType -> currentItem()) // this will work even if nothing is changed...
		{
			case 1:
				xMameBin = pthXmameSdlBin->text();
			break;
		
			case 2:
				xMameBin = pthXmameX11Bin -> text();
			break;
		
			case 3:
				xMameBin = pthXmameGlBin -> text();
			break;
			
			case 4:
				xMameBin = pthXmameSvgaLibBin -> text();
			break;
		}
		QString options;
		options = xMameBin;
		options +=" --version ";
		kdDebug() << "The version detection is executed with options: "<< options << endl;
                                                              
		nfoProc.clearArguments();
		#if KDE_VERSION >= 306
			nfoProc.setUseShell(true);
		#endif

		if (! xMameBin.isEmpty())
		{
			nfoProc << options.local8Bit();
			if (! nfoProc.start(KProcess::NotifyOnExit, KProcess::AllOutput))
				kdDebug() << "Cannot execute the version query for XMame; Options called:\n"<< options << endl;
		}
		else
		{
			if (xMameType -> currentItem() == 0)
				versionBrowser -> append(i18n("\nAutodetected:\n"));
			else
				versionBrowser -> append(i18n("\nUsing:\n"));
		
			versionBrowser -> append(i18n("The configured version of XMame does not exists..."));
		}
	}
}

void XMameCfg::modGeneral()
{
	loadGeneral = true;
	QFrame *page1 = addPage( i18n("General"), i18n("General"), locate("data","kmameleon/pics/mod_general.png") );
	QVBoxLayout *topLayout = new QVBoxLayout( page1, 0, 6 );
	QHBoxLayout *pickFile = new QHBoxLayout (page1,0,2);

	QLabel *label = new QLabel( i18n("XMame Type:"), page1 );
	QLabel *label2 = new QLabel( i18n("Configuration option:"), page1 );
	xMameType = new QComboBox(page1);
	chkOutput = new QCheckBox(i18n("Show output"), page1);
	chkScreenShots = new QCheckBox(i18n("Show screenshots"), page1);
	chkRestoreWindowPos = new QCheckBox(i18n("Restore window positions"), page1);
	chkCustomSwitch = new QCheckBox(i18n("Enable custom-only switches"), page1);
	chkShowListInstead = new QCheckBox(i18n("Use game list instead of dropdown box"), page1);

	chkColorOutput = new QCheckBox(i18n("Show colorful output"), page1);
	chkCheatMode = new QCheckBox(i18n("Enable cheat mode (Needs a cheat file)"), page1);
	leCheatFile = new QLineEdit(page1);
	leCheatFile->setReadOnly(true);

	btnFindCheatFile = new QToolButton(page1);
	btnFindCheatFile -> setText("...");
	pickFile -> addWidget(leCheatFile,0,0);
	pickFile -> addWidget(btnFindCheatFile,0,1);
	chkDetachProcess = new QCheckBox(i18n("Detach xmame process (it will close xmame process on kmameleon exit if left unchecked)"), page1);

//adding the XMame types to the list...
	xMameType->insertItem( i18n( "[autodetect]" ) );
	xMameType->insertItem( i18n( "SDL" ) );
	xMameType->insertItem( i18n( "X11" ) );
	xMameType->insertItem( i18n( "OpenGL" ) );
	xMameType->insertItem( i18n( "SVGALIB" ) );

	cbConfigType = new QComboBox(page1);
	cbConfigType->insertItem(i18n("Simple"));
	cbConfigType->insertItem(i18n("Advanced"));

	QLabel *lblInfo = new QLabel( i18n("Simple option lets You configure most of the basic things, that You need...\nAdvanced option will let You enable all the options... This is for advanced users only"), page1);

	topLayout->addWidget( label,0,0 );
	topLayout->addWidget( xMameType,1,0 );

	topLayout->addWidget( chkOutput,2,0 );
	topLayout->addWidget( chkColorOutput,3,0 );
	topLayout->addWidget( chkScreenShots,4,0 );
	topLayout->addWidget( chkCheatMode,5,0 );
	topLayout->addLayout( pickFile,6 );
	topLayout->addWidget( chkDetachProcess,7,0 );
	topLayout->addWidget( label2,8,0 );
	topLayout->addWidget( cbConfigType,9,0 );
	topLayout->addWidget( lblInfo,10,0 );
	topLayout->addWidget( chkCustomSwitch,11,0 );
	topLayout->addWidget( chkRestoreWindowPos,12,0 );
	topLayout->addWidget( chkShowListInstead,12,0 );
	
	connect (cbConfigType, SIGNAL(activated(int)), this, SLOT(slotConfigType(int)) );
	connect (btnFindCheatFile, SIGNAL(clicked()), this, SLOT(slotSetCheatFile()));
	connect (chkCustomSwitch, SIGNAL(clicked()), this, SLOT(lockLevelList()));
	connect (xMameType, SIGNAL(activated(int)), this, SLOT(xMameTypeChanged(int)) );

	QSpacerItem* spacer = new QSpacerItem( 0, 210, QSizePolicy::Minimum, QSizePolicy::Expanding );
	topLayout->addItem( spacer );
}

void XMameCfg::lockLevelList()
{
	if (chkCustomSwitch->isChecked())
		cbConfigType->setEnabled(false);
	else
		cbConfigType->setEnabled(true);
}

void XMameCfg::modAudio()
{
	loadAudio = true;
	QFrame *page = addPage( i18n("Audio"), i18n("Audio"), locate("data","kmameleon/pics/mod_audio.png") );
	QVBoxLayout *topLayout = new QVBoxLayout( page, 0, 6);

	chkIsSound = new QCheckBox(i18n("Enable XMame sound"), page);
	chkSamples = new QCheckBox(i18n("Enable samples (if avaliable)"), page);
	chkFakeSound = new QCheckBox(i18n("Enable fake sound (some games may not run without sound)"), page);

	topLayout->addWidget( chkIsSound,0,0 );
	topLayout->addWidget( chkSamples,1,0 );
	topLayout->addWidget( chkFakeSound,2,0 );

	QGroupBox *audioBox = new QGroupBox(page, "audioBox");
	audioBox->setTitle("Advanced audio options");
	QGridLayout* audioBoxLayout;

//Volume control
	audioBox->setColumnLayout(0, Qt::Vertical );
	audioBox->layout()->setSpacing( 6 );
	audioBox->layout()->setMargin( 11 );
	audioBoxLayout = new QGridLayout( audioBox->layout() );
	audioBoxLayout->setAlignment( Qt::AlignTop );

	audioSetVolume = new QSlider( audioBox, "audioSetVolume" );
	audioSetVolume->setMinValue( -32 );
	audioSetVolume->setMaxValue( 0 );
	audioSetVolume->setOrientation( QSlider::Horizontal );
	audioSetVolume->setTickmarks( QSlider::Left );
	audioSetVolume->setTickInterval( 5 );
	audioSetVolume->setValue( -16 );

	connect(audioSetVolume, SIGNAL(valueChanged(int)), this, SLOT(changeVolumeLevel(int)) );

	audioBoxLayout->addMultiCellWidget( audioSetVolume, 1, 1, 0, 8 );

	QLabel *lblVolume = new QLabel( audioBox, "lblVolume" );
	lblVolume->setText(i18n("Volume:"));
	audioBoxLayout->addMultiCellWidget( lblVolume, 0, 0, 0, 8 );

	QLabel *lblSofter = new QLabel( audioBox, "lblSofter" );
	lblSofter->setText(i18n("Softer"));
	audioBoxLayout->addWidget( lblSofter, 2, 0 );

	QLabel *lblLouder = new QLabel( audioBox, "lblLouder" );
	lblLouder->setText(i18n("Louder"));
	audioBoxLayout->addWidget( lblLouder, 2, 8 );

	QSpacerItem* spacer_2 = new QSpacerItem( 60, 21, QSizePolicy::Expanding, QSizePolicy::Minimum );
	audioBoxLayout->addItem( spacer_2, 2, 1 );
	QSpacerItem* spacer_3 = new QSpacerItem( 80, 21, QSizePolicy::Expanding, QSizePolicy::Minimum );
	audioBoxLayout->addItem( spacer_3, 2, 3 );

	lblDisplayVolume = new QLabel( audioBox, "lblDisplayVolume" );
	lblDisplayVolume->setText(i18n("-16 dB"));
	audioBoxLayout->addMultiCellWidget( lblDisplayVolume, 2, 2, 2, 3);

//everything else :)
	QFrame *line1 = new QFrame( audioBox, "line1" );
	line1->setFrameShape( QFrame::HLine );
	line1->setFrameShadow( QFrame::Sunken );
	line1->setFrameShape( QFrame::HLine );
	audioBoxLayout->addMultiCellWidget( line1, 3, 3, 0, 8 );

	QLabel *lblSound = new QLabel( audioBox, "lblSound" );
	lblSound->setText(i18n("Sound options"));
	audioBoxLayout->addMultiCellWidget( lblSound, 4, 4, 0, 1 );

	chkCustomAudioSf = new QCheckBox( audioBox, "chkCustomAudioSf" );
	chkCustomAudioSf->setText(i18n("Custom sample frequency"));
	audioBoxLayout->addMultiCellWidget( chkCustomAudioSf, 5, 5, 0, 3 );

	QLabel *lblDevices = new QLabel( audioBox, "lblDevices" );
	lblDevices->setText(i18n("Devices"));
	audioBoxLayout->addWidget( lblDevices, 4, 5 );


	chkCustomAudioMixerDevice = new QCheckBox( audioBox, "chkCustomAudioMixerDevice" );
	chkCustomAudioMixerDevice->setText(i18n("Custom mixer device"));
	audioBoxLayout->addMultiCellWidget( chkCustomAudioMixerDevice, 5, 5, 5, 8 );
	QSpacerItem* spacer_4 = new QSpacerItem( 20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding );
	audioBoxLayout->addItem( spacer_4, 7, 1 );

	chkCustomAudioBufSize = new QCheckBox( audioBox, "chkCustomAudioBufSize" );
	chkCustomAudioBufSize->setText(i18n("Custom sound frames to buffer"));
	audioBoxLayout->addMultiCellWidget( chkCustomAudioBufSize, 8, 8, 0, 3 );

	chkCustomAudioDevice = new QCheckBox( audioBox, "chkCustomAudioDevice" );
	chkCustomAudioDevice->setText(i18n("Custom audio device"));
	audioBoxLayout->addMultiCellWidget( chkCustomAudioDevice, 8, 8, 5, 8 );
	QSpacerItem* spacer_5 = new QSpacerItem( 20, 16, QSizePolicy::Minimum, QSizePolicy::Expanding );
	audioBoxLayout->addItem( spacer_5, 7, 6 );


	audioBufSizeEdit = new QLineEdit( audioBox, "audioBufSizeEdit" );
	audioBoxLayout->addMultiCellWidget( audioBufSizeEdit, 9, 9, 0, 2 );

	audioSfEdit = new QLineEdit( audioBox, "audioSfEdit" );
	audioBoxLayout->addMultiCellWidget( audioSfEdit, 6, 6, 0, 1 );

	QSpacerItem* spacer_6 = new QSpacerItem( 21, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
	audioBoxLayout->addItem( spacer_6, 9, 3 );
	QSpacerItem* spacer_7 = new QSpacerItem( 31, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
	audioBoxLayout->addItem( spacer_7, 6, 3 );

	audioMixerDeviceEdit = new QLineEdit( audioBox, "audioMixerDeviceEdit" );
	audioBoxLayout->addMultiCellWidget( audioMixerDeviceEdit, 6, 6, 5, 7 );

	audioDeviceEdit = new QLineEdit( audioBox, "audioDeviceEdit" );
	audioBoxLayout->addMultiCellWidget( audioDeviceEdit, 9, 9, 5, 7 );

	QSpacerItem* spacer_8 = new QSpacerItem( 21, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
	audioBoxLayout->addItem( spacer_8, 6, 8 );
	QSpacerItem* spacer_9 = new QSpacerItem( 20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
	audioBoxLayout->addItem( spacer_9, 9, 8 );

	topLayout->addWidget(audioBox,4,0);

//this  is on the very end... To keep "things up" :)
	QSpacerItem* spacer = new QSpacerItem( 0, 210, QSizePolicy::Minimum, QSizePolicy::Expanding );
	topLayout->addItem( spacer );
}

void XMameCfg::modVideo()
{
	loadVideo = true;
	QFrame *page = addPage( i18n("Video"), i18n("Video"), locate("data","kmameleon/pics/mod_video.png") );

	QVBoxLayout *topLayout = new QVBoxLayout( page, 0, 6 );
	//QHBoxLayout *layer_mf = new QHBoxLayout (page, 0, 0,"layer_mf");
	//QHBoxLayout *layer_mf2 = new QHBoxLayout (page, 0, 0,"layer_mf2");

	QLabel *lblVideoFx = new QLabel(i18n("Video effect:"), page);
	videoFx = new QComboBox(page);

	videoFx -> insertItem(i18n("none (default)"));
	videoFx -> insertItem(i18n("smooth scaling effect"));
	videoFx -> insertItem(i18n("low quality filter"));
	videoFx -> insertItem(i18n("high quality filter"));
	videoFx -> insertItem(i18n("6-tap filter with h-scanlines"));
	videoFx -> insertItem(i18n("light scanlines"));
	videoFx -> insertItem(i18n("rgb scanlines"));
	videoFx -> insertItem(i18n("deluxe scanlines"));
	videoFx -> insertItem(i18n("black scanlines"));

	topLayout->addWidget(lblVideoFx, 0, 0);
	topLayout->addWidget(videoFx, 1, 0);

	QLabel *lblBpp = new QLabel(i18n("Specify the colordepth the core should render:"), page);
	cbBpp = new QComboBox(page);
	cbBpp -> insertItem(i18n("Auto"));
	cbBpp -> insertItem(i18n("8"));
	cbBpp -> insertItem(i18n("16"));

	chkEnableFrameSkip = new QCheckBox(i18n("Enable frame skipping"), page);

	chkFrameSkip = new QCheckBox(i18n("Auto frame skipping"), page);
	QLabel *lblMaxAutoFrameSkip = new QLabel (i18n("Max auto frames skipped:"), page);
	spnMaxAutoFrameSkip = new QSpinBox(page);

	QLabel *lblNoAutoFrameSkip = new QLabel (i18n("Frameskip when not autoframeskiping:"), page);
	spnNoAutoFrameSkip = new QSpinBox(page);

//layout will go at the very end...
	topLayout->addWidget(lblBpp, 2, 0);
	topLayout->addWidget(cbBpp, 3, 0);
	topLayout->addWidget(chkEnableFrameSkip, 4, 0);
	topLayout->addWidget(chkFrameSkip, 5, 0);

	topLayout->addWidget(lblMaxAutoFrameSkip,0,0);
	QSpacerItem* xSpacer = new QSpacerItem( 111, 20, QSizePolicy::Minimum, QSizePolicy::Expanding );
	topLayout ->addItem(xSpacer);
	topLayout->addWidget(spnMaxAutoFrameSkip,1,0);

	topLayout->addWidget(lblNoAutoFrameSkip,0,0);
	QSpacerItem* xSpacer2 = new QSpacerItem( 111, 20, QSizePolicy::Minimum, QSizePolicy::Expanding );
	topLayout ->addItem(xSpacer2);
	topLayout->addWidget(spnNoAutoFrameSkip,1,0);

	QGridLayout* rotationBoxLayout;

	//topLayout->addLayout(layer_mf, 6);
	//topLayout->addLayout(layer_mf2, 7);

	rotationBox = new QGroupBox( page, "rotationBox" );
	rotationBox->setColumnLayout(0, Qt::Vertical );
	rotationBox->layout()->setSpacing( 4 );
	rotationBox->layout()->setMargin( 4 );
	rotationBoxLayout = new QGridLayout( rotationBox->layout() );
	rotationBoxLayout->setAlignment( Qt::AlignTop );
	rotationBox->setTitle( i18n( "Rotaton" ) );

	QHBoxLayout* layout5;
	layout5 = new QHBoxLayout( 0, 0, 4, "layout5");

	QVBoxLayout* layout26;
	layout26 = new QVBoxLayout( 0, 0, 4, "layout26");

	QVBoxLayout* layout24;
	layout24 = new QVBoxLayout( 0, 0, 4, "layout24");

	chkFlipX = new QCheckBox( rotationBox, "chkFlipX" );
	chkFlipX->setText( i18n( "Flip X Axis" ) );
	layout24->addWidget( chkFlipX );

	chkFlipY = new QCheckBox( rotationBox, "chkFlipY" );
	chkFlipY->setText( i18n( "Flip Y Axis" ) );
	layout24->addWidget( chkFlipY );
	layout26->addLayout( layout24 );
	QSpacerItem* spacer1 = new QSpacerItem( 20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding );
	layout26->addItem( spacer1 );
	layout5->addLayout( layout26 );
	QSpacerItem* spacer2 = new QSpacerItem( 140, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
	layout5->addItem( spacer2 );

	QVBoxLayout* layout25;
	layout25 = new QVBoxLayout( 0, 0, 4, "layout25");

	cbRotation = new QComboBox( FALSE, rotationBox, "cbRotation" );
	layout25->addWidget( cbRotation );
	QSpacerItem* spacer3 = new QSpacerItem( 20, 21, QSizePolicy::Minimum, QSizePolicy::Expanding );
	layout25->addItem( spacer3 );
	layout5->addLayout( layout25 );

	rotationBoxLayout->addLayout( layout5, 0, 0 );

	cbRotation->clear();
	cbRotation->insertItem( i18n( "Disable rotation" ) );
	cbRotation->insertItem( i18n( "No rotation (not the same as disable)" ) );
	cbRotation->insertItem( i18n( "Rotate 90 degrees left" ) );
	cbRotation->insertItem( i18n( "Rotate 90 degrees right" ) );
	topLayout->addWidget(rotationBox, 1, 0);

	QSpacerItem* spacer = new QSpacerItem( 0, 210, QSizePolicy::Minimum, QSizePolicy::Expanding );
	topLayout->addItem( spacer );
}

void XMameCfg::modGl()
{
	loadXGL = true;
	QFrame *page = addPage( i18n("GL xmame"), i18n("GL configuration"), locate("data","kmameleon/pics/mod_gl.png") );
	QGridLayout* pageLayout;
	QFrame* line1;
	QFrame* line2;
	QLabel* textLabel1;
	QLabel* textLabel2;
	QLabel* textLabel3;
	QLabel* textLabel4;
	QLabel* textLabel5;
	QLabel* textLabel6;

	pageLayout = new QGridLayout( page, 1, 1, 5, 5, "pageLayout"); 
	
	line2 = new QFrame( page, "line2" );
	line2->setFrameShape( QFrame::HLine );
	line2->setFrameShadow( QFrame::Sunken );
	line2->setFrameShape( QFrame::HLine );
	
	pageLayout->addMultiCellWidget( line2, 20, 20, 0, 2 );
	
	glchkGlExt = new QCheckBox( page, "glchkGlExt" );
	pageLayout->addMultiCellWidget( glchkGlExt, 5, 6, 0, 0 );
	
	glchkDblBuffer = new QCheckBox( page, "glchkDblBuffer" );
	pageLayout->addWidget( glchkDblBuffer, 2, 0 );
	
	glchkCMod = new QCheckBox( page, "glchkCMod" );
	pageLayout->addMultiCellWidget( glchkCMod, 13, 14, 0, 0 );
	
	glchkAA = new QCheckBox( page, "glchkAA" );
	pageLayout->addWidget( glchkAA, 17, 0 );
	
	glchkCabView = new QCheckBox( page, "glchkCabView" );
	pageLayout->addWidget( glchkCabView, 19, 0 );
	
	glchkAb = new QCheckBox( page, "glchkAb" );
	pageLayout->addMultiCellWidget( glchkAb, 15, 16, 0, 0 );
	
	glchkBitmap = new QCheckBox( page, "glchkBitmap" );
	pageLayout->addMultiCellWidget( glchkBitmap, 9, 10, 0, 0 );
	
	glchkBilinear = new QCheckBox( page, "glchkBilinear" );
	pageLayout->addMultiCellWidget( glchkBilinear, 7, 8, 0, 0 );
	
	glchkBlit = new QCheckBox( page, "glchkBlit" );
	pageLayout->addMultiCellWidget( glchkBlit, 3, 4, 0, 0 );
	
	glchkVAA = new QCheckBox( page, "glchkVAA" );
	pageLayout->addWidget( glchkVAA, 18, 0 );
	
	glchkVBitmap = new QCheckBox( page, "glchkVBitmap" );
	pageLayout->addMultiCellWidget( glchkVBitmap, 11, 12, 0, 0 );
	
	gllecabModel = new QLineEdit( page, "gllecabModel" );
	pageLayout->addMultiCellWidget( gllecabModel, 22, 22, 0, 2 );
	
	QSpacerItem* spacer = new QSpacerItem( 20, 16, QSizePolicy::Minimum, QSizePolicy::Expanding );
	pageLayout->addItem( spacer, 23, 0 );
	
	textLabel6 = new QLabel( page, "textLabel6" );
	pageLayout->addWidget( textLabel6, 21, 0 );
	
	glChkFullscreen = new QCheckBox( page, "glChkFullscreen" );
	pageLayout->addMultiCellWidget( glChkFullscreen, 0, 1, 0, 0 );
	
	line1 = new QFrame( page, "line1" );
	line1->setFrameShape( QFrame::VLine );
	line1->setFrameShadow( QFrame::Sunken );
	line1->setFrameShape( QFrame::VLine );
	pageLayout->addMultiCellWidget( line1, 0, 19, 1, 1 );
	
	textLabel1 = new QLabel( page, "textLabel1" );
	pageLayout->addWidget( textLabel1, 0, 2 );
	
	glleTexSize = new QLineEdit( page, "glleTexSize" );
	pageLayout->addMultiCellWidget( glleTexSize, 1, 2, 2, 2 );
	
	textLabel3 = new QLabel( page, "textLabel3" );
	pageLayout->addMultiCellWidget( textLabel3, 6, 7, 2, 2 );
	
	textLabel5 = new QLabel( page, "textLabel5" );
	pageLayout->addMultiCellWidget( textLabel5, 14, 15, 2, 2 );
	
	glleVBeamSize = new QLineEdit( page, "glleVBeamSize" );
	pageLayout->addMultiCellWidget( glleVBeamSize, 12, 13, 2, 2 );
	
	glleDGlLib = new QLineEdit( page, "glleDGlLib" );
	pageLayout->addMultiCellWidget( glleDGlLib, 4, 5, 2, 2 );
	
	textLabel4 = new QLabel( page, "textLabel4" );
	pageLayout->addMultiCellWidget( textLabel4, 10, 11, 2, 2 );
	
	textLabel2 = new QLabel( page, "textLabel2" );
	pageLayout->addWidget( textLabel2, 3, 2 );
	
	glleDGluLib = new QLineEdit( page, "glleDGluLib" );
	pageLayout->addMultiCellWidget( glleDGluLib, 8, 9, 2, 2 );
	
	glleXYRes = new QLineEdit( page, "glleXYRes" );
	pageLayout->addMultiCellWidget( glleXYRes, 16, 17, 2, 2 );

	glchkGlExt->setText( i18n( "Force usage of GL extension #78" ) );
	glchkDblBuffer->setText( i18n( "Enable / disable double buffer" ) );
	glchkCMod->setText( i18n( "Enable / disable color modulation" ) );
	glchkAA->setText( i18n( "Enable / disable antialiasing" ) );
	glchkCabView->setText( i18n( "Start / don't start in cabinet view mode" ) );
	glchkAb->setText( i18n( "Enable / disable alphablending (if avaliable)" ) );
	glchkBitmap->setText( i18n( "Enable / disable drawing of bitmap" ) );
	glchkBilinear->setText( i18n( "Enable / disable bilinear filtering" ) );
	glchkBlit->setText( i18n( "Force Blitter for True colors" ) );
	glchkVAA->setText( i18n( "Enable / disable vector aliasing" ) );
	glchkVBitmap->setText( i18n( "Enable / disable drawing of bitmap for vector games" ) );
	textLabel6->setText( i18n( "Cabinet model to use (def.: glmamejau) <string>:" ) );
	glChkFullscreen->setText( i18n( "Start in Fullscreen mode" ) );
	textLabel3->setText( i18n( "Dynamic GLU library <string>:" ) );
	textLabel4->setText( i18n( "Beam size for vector games <float>:" ) );
	textLabel2->setText( i18n( "Dynamic GL library <string>:" ) );
	textLabel5->setText( i18n( "Scale games to XRes x YRes <string>:" ) );
	textLabel1->setText( i18n( "GL texture size <int>:" ) );
}

void XMameCfg::modX11()
{
	loadX11 = true;
	QFrame *page6 = addPage( i18n("X11 xmame"), i18n("X11 configuration"), locate("data","kmameleon/pics/mod_x.png") );
	QGridLayout* Form1Layout;
	QSpacerItem* spacer2;
	QLabel* textLabel1;
	QFrame* line1;
	Form1Layout = new QGridLayout( page6, 1, 1, 0, 6, "Form1Layout"); 
	textLabel1 = new QLabel( page6, "textLabel1" );
	textLabel1->setText( i18n( "X11 mode:" ) );
	Form1Layout->addWidget( textLabel1, 0, 0 );
	
	cbX11WindowMode = new QComboBox( FALSE, page6, "cbX11WindowMode" );
	Form1Layout->addWidget( cbX11WindowMode, 1, 0 );
	cbX11WindowMode->clear();
	cbX11WindowMode->insertItem( i18n( "Normal windowed" ) );
	//cbX11WindowMode->insertItem( i18n( "DGA fullscreen" ) );
	cbX11WindowMode->insertItem( i18n( "Xv windowed" ) );
	cbX11WindowMode->insertItem( i18n( "Xv fullscreen" ) );
	
	line1 = new QFrame( page6, "line1" );
	line1->setFrameShape( QFrame::HLine );
	line1->setFrameShadow( QFrame::Sunken );
	line1->setFrameShape( QFrame::HLine );
	Form1Layout->addWidget( line1, 2, 0 );
	
	cbX11ForceYuv = new QCheckBox( page6, "cbX11ForceYuv" );
	cbX11ForceYuv->setText( i18n( "Force YUV mode" ) );
	QToolTip::add( cbX11ForceYuv, i18n( "Force YUV mode (for video cards with broken RGB hwscales)" ) );
	Form1Layout->addWidget( cbX11ForceYuv, 5, 0 );
	
	cbX11ForceYv12 = new QCheckBox( page6, "cbX11ForceYv12" );
	cbX11ForceYv12->setText( i18n( "Force YV12 mode" ) );
	QToolTip::add( cbX11ForceYv12, i18n( "Force YV12 mode (for video cards with broken RGB hwscales)" ) );
	Form1Layout->addWidget( cbX11ForceYv12, 6, 0 );
	
	cbX11MitShm = new QCheckBox( page6, "cbX11MitShm" );
	cbX11MitShm->setText( i18n( "Use/don't use MIT Shared Mem" ) );
	QToolTip::add( cbX11MitShm, i18n( "Use/don't use MIT Shared Mem (if available and compiled in)" ) );
	Form1Layout->addWidget( cbX11MitShm, 4, 0 );
	
	cbX11ShowCursor = new QCheckBox( page6, "cbX11ShowCursor" );
	cbX11ShowCursor->setText( i18n( "Show/don't show the cursor" ) );
	Form1Layout->addWidget( cbX11ShowCursor, 3, 0 );
	
	cbX11Widescreen = new QCheckBox( page6, "cbX11Widescreen" );
	cbX11Widescreen->setText( i18n( "Screen scales to 16:9" ) );
	QToolTip::add( cbX11Widescreen, QString::null );
	Form1Layout->addWidget( cbX11Widescreen, 7, 0 );
	
	cbX11XSync = new QCheckBox( page6, "cbX11XSync" );
	cbX11XSync->setText( i18n( "Use/don't use XSync" ) );
	QToolTip::add( cbX11XSync, i18n( "Use/don't use XSync instead of XFlush as screen refresh method" ) );
	Form1Layout->addWidget( cbX11XSync, 8, 0 );
	spacer2 = new QSpacerItem( 20, 61, QSizePolicy::Minimum, QSizePolicy::Expanding );
	Form1Layout->addItem( spacer2, 9, 0 );
}

void XMameCfg::modSdl()
{
	loadSdl = true;
	QFrame *page1 = addPage( i18n("SDL xmame"), i18n("SDL configuration"), locate("data","kmameleon/pics/mod_sdl.png") );
	QGridLayout* pageLayout;
	QLabel* textLabel1;
	QToolButton *btnGetSdlFsModes;
	
	pageLayout = new QGridLayout( page1, 1, 1, 0, 6, "pageLayout");
	
	chkSdlFullscreen = new QCheckBox( page1, "chkSdlFullscreen" );
	chkSdlFullscreen->setText( i18n( "Fullscreen" ) );
	pageLayout->addWidget( chkSdlFullscreen, 0, 0 );
	
	textLabel1 = new QLabel( page1, "textLabel1" );
	textLabel1->setText( i18n( "Try to use the fullscreen mode:" ) );
	pageLayout->addWidget( textLabel1, 1, 0 );
	
	cbSdlFsModes = new QComboBox( FALSE, page1, "cbSdlFsModes" );
	cbSdlFsModes->clear();
	cbSdlFsModes->insertItem( i18n( "Default" ) );
	pageLayout->addWidget( cbSdlFsModes, 2, 0 );
	
	QSpacerItem* spacer = new QSpacerItem( 20, 70, QSizePolicy::Minimum, QSizePolicy::Expanding );
	pageLayout->addItem( spacer, 4, 0 );
	
	btnGetSdlFsModes = new QToolButton( page1, "btnGetSdlFsModes" );
	btnGetSdlFsModes->setText( i18n( "Get fullscreen modes" ) );
	connect(btnGetSdlFsModes, SIGNAL( clicked() ), this, SLOT( slotGetSdlFsModes() ) );
	pageLayout->addWidget( btnGetSdlFsModes, 3, 0 );
}

void XMameCfg::modEffects()
{
	loadEffects = true;
	QFrame *page7 = addPage( i18n("Video FX"), i18n("Video Effects"), locate("data","kmameleon/pics/mod_fx.png") );
	QLabel* textLabel1;
	QLabel* textLabel2;
	QLabel* textLabel3;
	QLabel* textLabel4;
	QLabel* textLabel5;
	QLabel* textLabel6;
	QLabel* textLabel7;
	QLabel* textLabel8;
	QFrame* line1;
	QFrame* line3;
	QFrame* line5;
	QFrame* line4;

	QGridLayout* Form1Layout = new QGridLayout( page7, 1, 1, 5, 5, "Form1Layout"); 
	QHBoxLayout* layout1 = new QHBoxLayout( 0, 0, 6, "layout1"); 
	
	textLabel1 = new QLabel( page7, "textLabel1" );
	layout1->addWidget( textLabel1 );
	
	spnScaleVideo = new QSpinBox( page7, "spnScaleVideo" );
	layout1->addWidget( spnScaleVideo );
	Form1Layout->addMultiCellLayout( layout1, 14, 15, 0, 2 );
	
	chkSleepIdle = new QCheckBox( page7, "chkSleepIdle" );
	Form1Layout->addMultiCellWidget( chkSleepIdle, 12, 13, 0, 2 );
	
	chkThrottle = new QCheckBox( page7, "chkThrottle" );
	Form1Layout->addMultiCellWidget( chkThrottle, 10, 11, 0, 2 );
	
	chkArtCrop = new QCheckBox( page7, "chkArtCrop" );
	Form1Layout->addMultiCellWidget( chkArtCrop, 8, 9, 0, 2 );
	
	chkBezel = new QCheckBox( page7, "chkBezel" );
	Form1Layout->addMultiCellWidget( chkBezel, 6, 7, 0, 2 );
	
	chkOverlay = new QCheckBox( page7, "chkOverlay" );
	Form1Layout->addMultiCellWidget( chkOverlay, 5, 5, 0, 2 );
	
	chkBackdrops = new QCheckBox( page7, "chkBackdrops" );
	Form1Layout->addMultiCellWidget( chkBackdrops, 3, 4, 0, 2 );
	
	chkArtwork = new QCheckBox( page7, "chkArtwork" );
	Form1Layout->addMultiCellWidget( chkArtwork, 2, 2, 0, 2 );
	
	chkScanlines = new QCheckBox( page7, "chkScanlines" );
	Form1Layout->addMultiCellWidget( chkScanlines, 0, 1, 0, 2 );
	
	textLabel2 = new QLabel( page7, "textLabel2" );
	Form1Layout->addMultiCellWidget( textLabel2, 0, 0, 4, 5 );
	
	spnBrightness = new QSpinBox( page7, "spnBrightness" );
	spnBrightness->setMaxValue( 100 );
	
	Form1Layout->addWidget( spnBrightness, 1, 4 );
	QSpacerItem* spacer = new QSpacerItem( 171, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
	Form1Layout->addItem( spacer, 1, 5 );
	
	textLabel3 = new QLabel( page7, "textLabel3" );
	
	Form1Layout->addMultiCellWidget( textLabel3, 2, 3, 4, 5 );
	QSpacerItem* spacer_2 = new QSpacerItem( 170, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
	Form1Layout->addItem( spacer_2, 5, 5 );
	
	textLabel4 = new QLabel( page7, "textLabel4" );
	
	Form1Layout->addMultiCellWidget( textLabel4, 6, 6, 4, 5 );
	
	spnFrames = new QSpinBox( page7, "spnFrames" );
	
	Form1Layout->addMultiCellWidget( spnFrames, 4, 5, 4, 4 );
	
	spnYScale = new QSpinBox( page7, "spnYScale" );
	
	Form1Layout->addMultiCellWidget( spnYScale, 7, 8, 4, 4 );
	QSpacerItem* spacer_3 = new QSpacerItem( 181, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
	Form1Layout->addMultiCell( spacer_3, 7, 8, 5, 5 );
	
	textLabel5 = new QLabel( page7, "textLabel5" );
	
	Form1Layout->addMultiCellWidget( textLabel5, 9, 10, 4, 5 );
	QSpacerItem* spacer_4 = new QSpacerItem( 181, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
	Form1Layout->addMultiCell( spacer_4, 11, 12, 5, 5 );
	
	textLabel6 = new QLabel( page7, "textLabel6" );
	
	Form1Layout->addMultiCellWidget( textLabel6, 13, 14, 4, 5 );
	
	line1 = new QFrame( page7, "line1" );
	line1->setFrameShape( QFrame::VLine );
	line1->setFrameShadow( QFrame::Sunken );
	line1->setFrameShape( QFrame::VLine );
	
	Form1Layout->addMultiCellWidget( line1, 0, 15, 3, 3 );
	
	edtGamma = new QLineEdit( page7, "edtGamma" );
	
	Form1Layout->addMultiCellWidget( edtGamma, 15, 15, 4, 5 );
	
	spnXScale = new QSpinBox( page7, "spnXScale" );
	
	Form1Layout->addMultiCellWidget( spnXScale, 11, 12, 4, 4 );
	
	line3 = new QFrame( page7, "line3" );
	line3->setFrameShape( QFrame::HLine );
	line3->setFrameShadow( QFrame::Sunken );
	line3->setFrameShape( QFrame::HLine );
	
	Form1Layout->addMultiCellWidget( line3, 16, 16, 0, 5 );
	
	line5 = new QFrame( page7, "line5" );
	line5->setFrameShape( QFrame::HLine );
	line5->setFrameShadow( QFrame::Sunken );
	line5->setFrameShape( QFrame::HLine );
	Form1Layout->addMultiCellWidget( line5, 19, 19, 0, 5 );
	
	line4 = new QFrame( page7, "line4" );
	line4->setFrameShape( QFrame::VLine );
	line4->setFrameShadow( QFrame::Sunken );
	line4->setFrameShape( QFrame::VLine );
	Form1Layout->addMultiCellWidget( line4, 17, 18, 3, 3 );
	
	QSpacerItem* spacer_5 = new QSpacerItem( 20, 21, QSizePolicy::Minimum, QSizePolicy::Expanding );
	Form1Layout->addMultiCell( spacer_5, 20, 20, 2, 3 );
	
	textLabel7 = new QLabel( page7, "textLabel7" );
	Form1Layout->addMultiCellWidget( textLabel7, 17, 17, 0, 1 );
	
	spnArtResolution = new QSpinBox( page7, "spnArtResolution" );
	Form1Layout->addWidget( spnArtResolution, 18, 0 );
	
	QSpacerItem* spacer_6 = new QSpacerItem( 180, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
	Form1Layout->addItem( spacer_6, 18, 1 );
	
	textLabel8 = new QLabel( page7, "textLabel8" );
	Form1Layout->addMultiCellWidget( textLabel8, 17, 17, 4, 5 );
	
	edtAspectRatio = new QLineEdit( page7, "edtAspectRatio" );
	Form1Layout->addMultiCellWidget( edtAspectRatio, 18, 18, 4, 5 );
	
	textLabel1->setText( i18n( "Scale video to exactly this \nheight: (0 = disable)" ) );
	chkSleepIdle->setText( i18n( "Enable / disable sleep during idle" ) );
	chkThrottle->setText( i18n( "Enable / disable throttle" ) );
	chkArtCrop->setText( i18n( "Crop artwork to game screen only" ) );
	chkBezel->setText( i18n( "Use Bezel artwork" ) );
	chkOverlay->setText( i18n( "Use overlay artwork" ) );
	chkBackdrops->setText( i18n( "Use backdrop artwork" ) );
	chkArtwork->setText( i18n( "Use additional game artwork" ) );
	chkScanlines->setText( i18n( "Enable / disable displaying \nsimulated scanlines" ) );
	textLabel2->setText( i18n( "Brightness: (0-100%)" ) );
	textLabel3->setText( i18n( "Number of frames to run within the \ngame:" ) );
	textLabel4->setText( i18n( "Y-Scale aspect ratio:" ) );
	textLabel5->setText( i18n( "X-Scale aspect ratio:" ) );
	textLabel6->setText( i18n( "Gamma correction: (0.5 - 2.0)" ) );
	textLabel7->setText( i18n( "Artwork resolution (0 for auto)" ) );
	textLabel8->setText( i18n( "X-Y Scale to the same aspect ratio:" ) );
}

void XMameCfg::modDevices()
{
	loadDevices = true;
	QFrame *page6 = addPage( i18n("Devices"), i18n("Devices"), locate("data","kmameleon/pics/mod_devices.png") );
	QGridLayout* Form1Layout = new QGridLayout( page6, 1, 1, 5, 5, "Form1Layout"); 
	
	inputBox = new QComboBox( FALSE, page6, "inputBox" );
	Form1Layout->addWidget( inputBox, 1, 0 );
	
	chkAnalogStick = new QCheckBox( page6, "chkAnalogStick" );
	Form1Layout->addWidget( chkAnalogStick, 2, 0 );
	
	chkRapidFire = new QCheckBox( page6, "chkRapidFire" );
	Form1Layout->addWidget( chkRapidFire, 8, 0 );
	
	customJoyDevName = new QLineEdit( page6, "customJoyDevName" );
	Form1Layout->addWidget( customJoyDevName, 4, 0 );
	
	chkGrabMouse = new QCheckBox( page6, "chkGrabMouse" );
	Form1Layout->addWidget( chkGrabMouse, 9, 0 );
	
	QLabel *textLabel1 = new QLabel( page6, "textLabel1" );
	Form1Layout->addWidget( textLabel1, 3, 0 );
	
	chkUseHotRodSe = new QCheckBox( page6, "chkUseHotRodSe" );
	Form1Layout->addWidget( chkUseHotRodSe, 6, 0 );
	
	chkUsbPsPad = new QCheckBox( page6, "chkUsbPsPad" );
	Form1Layout->addWidget( chkUsbPsPad, 7, 0 );
	
	QLabel *textLabel1_2 = new QLabel( page6, "textLabel1_2" );
	Form1Layout->addWidget( textLabel1_2, 0, 0 );
	
	chkUseHotRod = new QCheckBox( page6, "chkUseHotRod" );
	Form1Layout->addWidget( chkUseHotRod, 5, 0 );
	QSpacerItem* spacer = new QSpacerItem( 20, 91, QSizePolicy::Minimum, QSizePolicy::Expanding );
	Form1Layout->addItem( spacer, 10, 0 );

//data...
	inputBox->clear();
	inputBox->insertItem( i18n( "No joystick" ) );
	inputBox->insertItem( i18n( "i386 style joystick driver (if compiled in)" ) );
	inputBox->insertItem( i18n( "Fm Town Pad support (if compiled in)" ) );
	inputBox->insertItem( i18n( "X11 input extension joystick (if compiled in)" ) );
	inputBox->insertItem( i18n( "New i386 linux 1.x.x joystick driver (if compiled in)" ) );
	inputBox->insertItem( i18n( "NetBSD USB joystick driver (if compiled in)" ) );
	chkAnalogStick->setText( i18n( "Use joystick as analog for analog controls" ) );
	chkRapidFire->setText( i18n( "Enable rapid-fire support for joystics" ) );
	chkGrabMouse->setText( i18n( "Grab mouse (Good idea for XGL xmame)" ) );
	textLabel1->setText( i18n( "Joystick device prefix:" ) );
	chkUseHotRodSe->setText( i18n( "Enable HotRod SE joystic support" ) );
	chkUsbPsPad->setText( i18n( "The joystick(s) are USB PS Game Pads" ) );
	textLabel1_2->setText( i18n( "Select joystick type:" ) );
	chkUseHotRod->setText( i18n( "Enable HotRod joystic support" ) );
}

void XMameCfg::modCustom()
{
	loadCustom = true;
	QFrame *page9 = addPage( i18n("Custom switches"), i18n("Custom switches"), locate("data","kmameleon/pics/mod_custom.png") );
	QGridLayout *Form1Layout = new QGridLayout( page9, 1, 1, 5, 5, "Form1Layout"); 
	QLabel *textLabel1 = new QLabel( page9, "textLabel1" );
	textLabel1->setText( i18n( "Custom commands / switches" ) );
	Form1Layout->addWidget( textLabel1, 0, 0 );
	customEditor = new QTextEdit( page9, "textEdit1" );
	Form1Layout->addWidget( customEditor, 1, 0 );
	QPushButton *btnGCV = new QPushButton( page9, "btnGCV" );
	btnGCV->setText( i18n( "Get current values" ) );
	Form1Layout->addWidget( btnGCV, 2, 0 );
	
	connect(btnGCV, SIGNAL( clicked() ), this, SLOT( loadCurrentParameters()) );
}

void XMameCfg::modPaths()
{
	loadPaths = true;
	QFrame *page3 = addPage( i18n("Paths"), i18n("Paths"), locate("data","kmameleon/pics/mod_paths.png") );
	QGridLayout* Form1Layout = new QGridLayout( page3, 1, 1, 0, 0, "Form1Layout"); 
	QToolButton *btnRedetect = new QToolButton( page3, "btnRedetect" );
	Form1Layout->addWidget( btnRedetect, 8, 0 );
	
	QVBoxLayout* layout33 = new QVBoxLayout( 0, 0, 0, "layout33"); 
	QLabel *textLabel8 = new QLabel( page3, "textLabel8" );
	layout33->addWidget( textLabel8 );
	
	QHBoxLayout* layout32 = new QHBoxLayout( 0, 0, 0, "layout32"); 
	
	pthScreenShots = new QLineEdit( page3, "pthScreenShots" );
	pthScreenShots->setReadOnly( TRUE );
	layout32->addWidget( pthScreenShots );
	
	QToolButton *btnSelSSDirectory = new QToolButton( page3, "btnSelSSDirectory" );
	layout32->addWidget( btnSelSSDirectory );
	layout33->addLayout( layout32 );
	
	Form1Layout->addLayout( layout33, 7, 0 );
	
	QVBoxLayout* layout34 = new QVBoxLayout( 0, 0, 0, "layout34"); 
	
	QLabel *textLabel7 = new QLabel( page3, "textLabel7" );
	layout34->addWidget( textLabel7 );
	
	QHBoxLayout* layout31 = new QHBoxLayout( 0, 0, 0, "layout31"); 
	
	pthXmameSvgaLibBin = new QLineEdit( page3, "pthXmameSvgaLibBin" );
	pthXmameSvgaLibBin->setReadOnly( TRUE );
	layout31->addWidget( pthXmameSvgaLibBin );
	
	QToolButton *btnSelSvgaLibBin = new QToolButton( page3, "btnSelSvgaLibBin" );
	layout31->addWidget( btnSelSvgaLibBin );
	layout34->addLayout( layout31 );
	
	Form1Layout->addLayout( layout34, 6, 0 );
	
	QVBoxLayout* layout35 = new QVBoxLayout( 0, 0, 0, "layout35"); 
	
	QLabel *textLabel6 = new QLabel( page3, "textLabel6" );
	layout35->addWidget( textLabel6 );
	
	QHBoxLayout* layout30 = new QHBoxLayout( 0, 0, 0, "layout30"); 
	
	pthXmameGlBin = new QLineEdit( page3, "pthXmameGlBin" );
	pthXmameGlBin->setReadOnly( TRUE );
	layout30->addWidget( pthXmameGlBin );
	
	QToolButton *btnSelGlBin = new QToolButton( page3, "btnSelGlBin" );
	layout30->addWidget( btnSelGlBin );
	layout35->addLayout( layout30 );
	
	Form1Layout->addLayout( layout35, 5, 0 );
	
	QVBoxLayout* layout36 = new QVBoxLayout( 0, 0, 0, "layout36"); 
	
	QLabel *textLabel5 = new QLabel( page3, "textLabel5" );
	layout36->addWidget( textLabel5 );
	
	QHBoxLayout* layout29 = new QHBoxLayout( 0, 0, 0, "layout29"); 
	
	pthXmameX11Bin = new QLineEdit( page3, "pthXmameX11Bin" );
	pthXmameX11Bin->setReadOnly( TRUE );
	layout29->addWidget( pthXmameX11Bin );
	
	QToolButton *btnSelX11Bin = new QToolButton( page3, "btnSelX11Bin" );
	layout29->addWidget( btnSelX11Bin );
	layout36->addLayout( layout29 );
	
	Form1Layout->addLayout( layout36, 4, 0 );
	
	QVBoxLayout* layout37 = new QVBoxLayout( 0, 0, 0, "layout37"); 
	
	QLabel *textLabel4 = new QLabel( page3, "textLabel4" );
	layout37->addWidget( textLabel4 );
	
	QHBoxLayout* layout28 = new QHBoxLayout( 0, 0, 0, "layout28"); 
	
	pthXmameSdlBin = new QLineEdit( page3, "pthXmameSdlBin" );
	pthXmameSdlBin->setReadOnly( TRUE );
	layout28->addWidget( pthXmameSdlBin );
	
	QToolButton *btnSelSdlBin = new QToolButton( page3, "btnSelSdlBin" );
	layout28->addWidget( btnSelSdlBin );
	layout37->addLayout( layout28 );
	
	Form1Layout->addLayout( layout37, 3, 0 );
	
	QVBoxLayout* layout38 = new QVBoxLayout( 0, 0, 0, "layout38"); 
	
	QLabel *textLabel3 = new QLabel( page3, "textLabel3" );
	layout38->addWidget( textLabel3 );
	
	QHBoxLayout* layout27 = new QHBoxLayout( 0, 0, 0, "layout27"); 
	
	pthXmameArt = new QLineEdit( page3, "pthXmameArt" );
	pthXmameArt->setReadOnly( TRUE );
	layout27->addWidget( pthXmameArt );
	
	QToolButton *btnSelArt = new QToolButton( page3, "btnSelArt" );
	layout27->addWidget( btnSelArt );
	layout38->addLayout( layout27 );
	
	Form1Layout->addLayout( layout38, 2, 0 );
	
	QVBoxLayout* layout39 = new QVBoxLayout( 0, 0, 0, "layout39"); 
	
	QLabel *textLabel2 = new QLabel( page3, "textLabel2" );
	layout39->addWidget( textLabel2 );
	
	QHBoxLayout* layout26 = new QHBoxLayout( 0, 0, 0, "layout26"); 
	
	pthXmameSamples = new QLineEdit( page3, "pthXmameSamples" );
	pthXmameSamples->setReadOnly( TRUE );
	layout26->addWidget( pthXmameSamples );
	
	QToolButton *btnSelSamples = new QToolButton( page3, "btnSelSamples" );
	layout26->addWidget( btnSelSamples );
	layout39->addLayout( layout26 );
	
	Form1Layout->addLayout( layout39, 1, 0 );
	
	QVBoxLayout* layout40 = new QVBoxLayout( 0, 0, 0, "layout40"); 
	
	QLabel *textLabel1 = new QLabel( page3, "textLabel1" );
	layout40->addWidget( textLabel1 );
	
	QHBoxLayout* layout25 = new QHBoxLayout( 0, 0, 0, "layout25"); 
	
	pthXmameRoms = new QLineEdit( page3, "pthXmameRoms" );
	pthXmameRoms->setReadOnly( TRUE );
	layout25->addWidget( pthXmameRoms );
	
	QToolButton *btnSelRoms = new QToolButton( page3, "btnSelRoms" );
	layout25->addWidget( btnSelRoms );
	layout40->addLayout( layout25 );
	
	Form1Layout->addLayout( layout40, 0, 0 );
	QSpacerItem* spacer6 = new QSpacerItem( 20, 100, QSizePolicy::Minimum, QSizePolicy::Expanding );
	Form1Layout->addItem( spacer6, 9, 0 );

	btnRedetect->setText( i18n( "Redetect values" ) );
	btnSelSSDirectory->setText( i18n( "..." ) );
	btnSelSvgaLibBin->setText( i18n( "..." ) );
	btnSelGlBin->setText( i18n( "..." ) );
	btnSelX11Bin->setText( i18n( "..." ) );
	btnSelSdlBin->setText( i18n( "..." ) );
	btnSelArt->setText( i18n( "..." ) );
	btnSelSamples->setText( i18n( "..." ) );
	btnSelRoms->setText( i18n( "..." ) );
	textLabel8->setText( i18n( "Screenshots directory:" ) );
	textLabel7->setText( i18n( "XMame SvgaLib binary location:" ) );
	textLabel6->setText( i18n( "XMame OpenGL binary location:" ) );
	textLabel5->setText( i18n( "XMame X11 binary location:" ) );
	textLabel4->setText( i18n( "XMame SDL binary location:" ) );
	textLabel3->setText( i18n( "XMame artwork path:" ) );
	textLabel2->setText( i18n( "XMame samples path:" ) );
	textLabel1->setText( i18n( "XMame ROM path:" ) );
	
//connections...
	connect(btnSelSdlBin, SIGNAL(clicked()), this, SLOT(slotSetSdlMameBin()) );
	connect(btnSelX11Bin, SIGNAL(clicked()), this, SLOT(slotSetX11MameBin()) );
	connect(btnSelGlBin, SIGNAL(clicked()), this, SLOT(slotSetGlMameBin()) );
	connect(btnSelSvgaLibBin, SIGNAL(clicked()), this, SLOT(slotSetSvgaLibMameBin()) );
	connect(btnSelRoms, SIGNAL(clicked()), this, SLOT(slotSetXMameRomDir()) );
	connect(btnSelSamples, SIGNAL(clicked()), this, SLOT(slotSetXMameSamplesDir()) );
	connect(btnSelArt, SIGNAL(clicked()), this, SLOT(slotSetXMameArtDir()) );
	connect(btnSelSSDirectory, SIGNAL(clicked()), this, SLOT(slotSetSSDir()) );
	connect(btnRedetect, SIGNAL(clicked()), this, SLOT(reload()) );
}

void XMameCfg::slotSetSdlMameBin()
{
	QString binDir = KFileDialog::getExistingDirectory(pthXmameSdlBin->text(), this, i18n("Select XMame SDL binary directory"));
	if (! binDir.isEmpty())
	{
		QFileInfo fi(binDir+"/xmame.SDL");
		if (fi.exists())
			pthXmameSdlBin->setText(binDir+"/xmame.SDL");
		else
			KMessageBox::sorry(this,i18n("I can't locate XMame SDL binaries at that location..."));
	}
}

void XMameCfg::slotSetX11MameBin()
{
	QString binDir = KFileDialog::getExistingDirectory(pthXmameX11Bin->text(), this, i18n("Select XMame X11 binary directory"));
	if (! binDir.isEmpty())
	{
		QFileInfo fi(binDir+"/xmame.x11");
		if (fi.exists())
			pthXmameX11Bin->setText(binDir+"/xmame.x11");
		else
			KMessageBox::sorry(this,i18n("I can't locate XMame X11 binaries at that location..."));
	}
}

void XMameCfg::slotSetGlMameBin()
{
	QString binDir = KFileDialog::getExistingDirectory(pthXmameGlBin->text(), this, i18n("Select XMame XGL binary directory"));
	if (! binDir.isEmpty())
	{
		QFileInfo fi(binDir+"/xmame.xgl");
		if (fi.exists())
			pthXmameGlBin->setText(binDir+"/xmame.xgl");
		else
			KMessageBox::sorry(this,i18n("I can't locate XMame XGL binaries at that location..."));
	}
}

void XMameCfg::slotSetSvgaLibMameBin()
{
	QString binDir = KFileDialog::getExistingDirectory(pthXmameSvgaLibBin->text(), this, i18n("Select XMame X11 binary directory"));
	if (! binDir.isEmpty())
	{
		QFileInfo fi(binDir+"xmame.svgalib");
		if (fi.exists())
			pthXmameSvgaLibBin->setText(binDir+"/xmame.svgalib");
		else
			KMessageBox::sorry(this,i18n("I can't locate XMame SVGALIB binaries at that location..."));
	}
}

void XMameCfg::slotSetXMameRomDir()
{
	QString romDir = KFileDialog::getExistingDirectory(pthXmameRoms->text(), this, i18n("Select XMame ROMS directory"));
	if (! romDir.isEmpty())
		pthXmameRoms->setText(romDir);
}

void XMameCfg::slotSetXMameSamplesDir()
{
	QString samplesDir = KFileDialog::getExistingDirectory(pthXmameSamples->text(), this, i18n("Select XMame samples directory"));
	if (! samplesDir.isEmpty())
		pthXmameSamples->setText(samplesDir);
}

void XMameCfg::slotSetXMameArtDir()
{
	QString artDir = KFileDialog::getExistingDirectory(pthXmameArt->text(), this, i18n("Select XMame artwork directory"));
	if (! artDir.isEmpty())
		pthXmameArt->setText(artDir);
}

void XMameCfg::slotSetSSDir()
{
	QString ssDir = KFileDialog::getExistingDirectory(pthScreenShots->text(), this, i18n("Select XMame snapshots directory"));
	if (! ssDir.isEmpty())
		pthScreenShots->setText(ssDir);
}

void XMameCfg::modFirstRun()
{
	chkOutput->setChecked(false);
	chkIsSound->setChecked(true);
	
	config->setGroup("general");
	config->writeEntry("restoreWindowPosition",false);
	config->writeEntry("displayType",false);

	config->writeEntry("showOutput",true);

	reload();
	modInfoReload();
/*
//Default XMame path...
	config->setGroup("general");
	config->writeEntry("xmame_sdl",pthXmameSdlBin->text());
	config->writeEntry("xmame_x11",pthXmameX11Bin->text());
	config->writeEntry("xmame_gl",pthXmameGlBin->text());
	config->writeEntry("xmame_svgalib",pthXmameSvgaLibBin->text());

	config->setGroup("audio");
	config->writeEntry("issound",true);
	config->writeEntry("volume",-16);
	
//first run...
	config->setGroup("First_run");
	config->writeEntry("First",false);

	config->sync();*/
}

void XMameCfg::reload() //needed, because firstRun shouldn't set the rest of the vars than path back...
{
	kdDebug() << "reload() loaded" << endl;
	QStringList xMameBinList;
	//this will be overwritten unconditinally
	if (pthXmameSdlBin -> text().isEmpty())
		pthXmameSdlBin->setText(KStandardDirs::findExe("xmame.SDL"));
	else if (KMessageBox::questionYesNo(this,i18n("This will override your current SDL binary path... Want me to proceed?")) == 3)
		pthXmameSdlBin->setText(KStandardDirs::findExe("xmame.SDL"));

	if (pthXmameX11Bin -> text().isEmpty())
		pthXmameX11Bin->setText(KStandardDirs::findExe("xmame.x11"));
	else if (KMessageBox::questionYesNo(this,i18n("This will override your current X11 binary path... Want me to proceed?")) == 3)
		pthXmameX11Bin->setText(KStandardDirs::findExe("xmame.x11"));

	if (pthXmameGlBin -> text().isEmpty()) {
		pthXmameGlBin->setText(KStandardDirs::findExe("xmame.xgl"));
	} else if (KMessageBox::questionYesNo(this,i18n("This will override your current XGL binary path... Want me to proceed?")) == 3) {
		pthXmameGlBin->setText(KStandardDirs::findExe("xmame.xgl"));
	}
	if (pthXmameSvgaLibBin -> text().isEmpty()) {
		pthXmameSvgaLibBin->setText(KStandardDirs::findExe("xmame.svgalib"));
	} else if (KMessageBox::questionYesNo(this,i18n("This will override your current SVGALIB binary path... Want me to proceed?")) == 3) {
		pthXmameSvgaLibBin->setText(KStandardDirs::findExe("xmame.svgalib"));
	}

//could this be easier?
	xMameBinList.clear();
	if (! pthXmameSdlBin -> text().isEmpty()) {
		xMameBinList +=pthXmameSdlBin -> text();
	}
	if (! pthXmameX11Bin -> text().isEmpty()) {
		xMameBinList +=pthXmameX11Bin -> text();
	}
	if (! pthXmameGlBin -> text().isEmpty()) {
		xMameBinList +=pthXmameGlBin -> text();
	}
	if (! pthXmameSvgaLibBin -> text().isEmpty()) {
		xMameBinList +=pthXmameSvgaLibBin -> text();
	}
	kdDebug() << "Executables count [listed]: " << xMameBinList.count() << endl;

	if ( xMameBinList.count() < 1) {
		KMessageBox::information(this, i18n("Cannot find XMame binaries... Please make sure that it is installed..."));
	} else if ( xMameBinList.count() > 1) {
		KMessageBox::information(this, i18n("KMameleon found more than 1 XMame binary file.\nPlease use PathFinder tool to set the default rompath/artwork/samples directory or set it manually."));
	}
	else if ( xMameBinList.count() == 1 )
	{
		kdDebug() << xMameBinList[0] << endl;
		QString options = xMameBinList[0];
		options +=" -sc ";
		options +=" | grep -i / ";

		adProc.clearArguments();
		#if KDE_VERSION >= 306
			adProc.setUseShell(true);
		#endif
		
		adProc << options.local8Bit();
		if (! adProc.start(KProcess::Block, (KProcess::Communication)(KProcess::Stdout))) {
			kdDebug() <<"Can't exec the process..." << endl;
		} else {
			adProc.resume();
		}
		
		/*
		if (! adProc.start(KProcess::NotifyOnExit, KProcess::AllOutput))
			kdDebug() << "Cannot execute the configuration query for XMame; Options called:\n"<< options << endl;
		else
			kdDebug() << "Process running" << endl;
*/
	}
}

void XMameCfg::modThemes()
{
	loadThemeMgr = true;
	QFrame *page1 = addPage( i18n("Themes"), i18n("Color themes"), locate("data","kmameleon/pics/mod_themes.png") );

	
	QLabel* textLabel2;
	QToolButton* btnImageBg;

	QGridLayout* Form1Layout;
	QSpacerItem* spacer3;
	QGridLayout* layout6;
	QHBoxLayout* layout5;
	QSpacerItem* spacer4;
	
	
	Form1Layout = new QGridLayout( page1, 1, 1, 11, 6, "Form1Layout"); 
	spacer3 = new QSpacerItem( 31, 81, QSizePolicy::Minimum, QSizePolicy::Expanding );
	Form1Layout->addItem( spacer3, 2, 0 );

	layout6 = new QGridLayout( 0, 1, 1, 0, 6, "layout6"); 

	textLabel2 = new QLabel( page1, "textLabel2" );
	textLabel2->setText( i18n( "Select background image:" ) );

	layout6->addWidget( textLabel2, 0, 0 );

	layout5 = new QHBoxLayout( 0, 0, 6, "layout5"); 

	leBgImage = new QLineEdit( page1, "leBgImage" );
	layout5->addWidget( leBgImage );

	btnImageBg = new QToolButton( page1, "btnImageBg" );
	btnImageBg->setText( i18n( "..." ) );
	layout5->addWidget( btnImageBg );
	spacer4 = new QSpacerItem( 21, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
	layout5->addItem( spacer4 );

	layout6->addLayout( layout5, 1, 0 );

	Form1Layout->addLayout( layout6, 1, 0 );
}

void XMameCfg::loadConfig()
{
	if (loadGeneral)
	{
		config->setGroup("general");
		pthXmameRoms->setText(config->readEntry("xmame_rom",""));
		pthXmameSamples->setText(config->readEntry("xmame_samples",""));
		pthXmameArt->setText(config->readEntry("xmame_art",""));
		pthScreenShots->setText(config->readEntry("xmame_screenshots",""));

		pthXmameSdlBin->setText(config->readEntry("xmame_sdl",""));
		pthXmameX11Bin->setText(config->readEntry("xmame_x11",""));
		pthXmameGlBin->setText(config->readEntry("xmame_gl",""));
		pthXmameSvgaLibBin->setText(config->readEntry("xmame_svga",""));

		chkOutput->setChecked(config->readBoolEntry("showOutput",false));
		chkColorOutput->setChecked(config->readBoolEntry("color_debug",false));
		chkScreenShots->setChecked(config->readBoolEntry("showScreenshots",false));
		cbConfigType->setCurrentItem(config->readNumEntry("setmode", 0));
		chkCheatMode->setChecked(config->readBoolEntry("cheatMode", false));
		leCheatFile->setText(config->readEntry("cheatFile", ""));
		chkDetachProcess->setChecked(config->readBoolEntry("detachProcess", false));

		xMameType->setCurrentItem(config->readNumEntry("xmame_type",0));
		chkCustomSwitch->setChecked(config->readBoolEntry("custom", false));
		chkRestoreWindowPos->setChecked(config->readBoolEntry("restoreWindowPosition",true));
		chkShowListInstead->setChecked(config->readBoolEntry("showList", true));
		lockLevelList();
	}

	if (loadAudio)
	{
		config->setGroup("audio");
		chkIsSound->setChecked(config->readBoolEntry("issound", true));
		chkSamples->setChecked(config->readBoolEntry("samples", false));
		chkFakeSound->setChecked(config->readBoolEntry("fakesound", false));
		audioSetVolume->setValue(config->readNumEntry("volume",-16));

		chkCustomAudioSf->setChecked(config->readBoolEntry("customsamplefrequency",false));
		chkCustomAudioBufSize->setChecked(config->readBoolEntry("custombufsize",false));
		chkCustomAudioDevice->setChecked(config->readBoolEntry("customaudiodevice",false));
		chkCustomAudioMixerDevice->setChecked(config->readBoolEntry("custommixerdevice",false));

		audioSfEdit->setText(config->readEntry("samplefrequency",""));
		audioBufSizeEdit->setText(config->readEntry("bufsize",""));
		audioDeviceEdit->setText(config->readEntry("audiodevice",""));
		audioMixerDeviceEdit->setText(config->readEntry("mixerdevice",""));
	}

	if (loadVideo)
	{
		config->setGroup("video");
		videoFx->setCurrentItem(config->readNumEntry("effect",0));
		cbRotation->setCurrentItem(config->readNumEntry("rotation",0));
		spnMaxAutoFrameSkip->setValue(config->readNumEntry("maxautoframeskip",0));
		spnNoAutoFrameSkip->setValue(config->readNumEntry("noautoframeskip",0));
		chkEnableFrameSkip->setChecked(config->readBoolEntry("enableframeskip",false));
		chkFrameSkip->setChecked(config->readBoolEntry("enableautoframeskip",false));
		chkFlipX->setChecked(config->readBoolEntry("flipx",false));
		chkFlipY->setChecked(config->readBoolEntry("flipy",false));
	}

	if (loadXGL)
	{
		config->setGroup("XGL");
		glleTexSize->setText(config->readEntry("glTexSize", ""));
		glleDGlLib->setText(config->readEntry("glDGlLib",""));
		glleDGluLib->setText(config->readEntry("glDGluLib",""));
		glleVBeamSize->setText(config->readEntry("glVBeamSize",""));
		gllecabModel->setText(config->readEntry("glcabModel",""));
		glleXYRes->setText(config->readEntry("glXYRes",""));

		glChkFullscreen->setChecked(config->readBoolEntry("glFullscreen",false));
		glchkDblBuffer->setChecked(config->readBoolEntry("glDblBuffer",true));
		glchkBlit->setChecked(config->readBoolEntry("glBlit",true));
		glchkGlExt->setChecked(config->readBoolEntry("glGlExt",true));
		glchkBilinear->setChecked(config->readBoolEntry("glBilinear",true));
		glchkBitmap->setChecked(config->readBoolEntry("glBitmap",true));
		glchkVBitmap->setChecked(config->readBoolEntry("glVBitmap",true));
		glchkCMod->setChecked(config->readBoolEntry("glCMod",true));
		glchkAb->setChecked(config->readBoolEntry("glAb",true));
		glchkAA->setChecked(config->readBoolEntry("glAA",true));
		glchkVAA->setChecked(config->readBoolEntry("glVAA",true));
		glchkCabView->setChecked(config->readBoolEntry("glCabView",false));
	}

	if (loadEffects)
	{
		config->setGroup("effects");
		chkScanlines->setChecked(config->readBoolEntry("enablescanlines",false));
		chkArtwork->setChecked(config->readBoolEntry("enableartwork",false));
		chkOverlay->setChecked(config->readBoolEntry("enableoverlays",false));
		chkBezel->setChecked(config->readBoolEntry("enablebezel",false));
		chkArtCrop->setChecked(config->readBoolEntry("enableartcrop",false));
		chkThrottle->setChecked(config->readBoolEntry("enablethrottle",false));
		chkSleepIdle->setChecked(config->readBoolEntry("enablesleepidle",false));

		spnBrightness->setValue(config->readNumEntry("brighteness",0));
		spnFrames->setValue(config->readNumEntry("fframes",0));
		spnXScale->setValue(config->readNumEntry("xscale",0));
		spnYScale->setValue(config->readNumEntry("yscale",0));
		spnScaleVideo->setValue(config->readNumEntry("scalevideo",0));
		spnArtResolution->setValue(config->readNumEntry("artres",0));

		edtGamma->setText(config->readEntry("gamma",""));
		edtAspectRatio->setText(config->readEntry("aspectratio",""));
	}

	if (loadDevices)
	{
		config->setGroup("devices");
		inputBox->setCurrentItem(config->readNumEntry("JoystickType",0));
		chkAnalogStick->setChecked(config->readBoolEntry("AnalogStick", false));
		customJoyDevName->setText(config->readEntry("JoyDevice", "/dev/js"));
		chkUseHotRod->setChecked(config->readBoolEntry("UseHotRod", false));
		chkUseHotRodSe->setChecked(config->readBoolEntry("UseHotRodSe", false));
		chkUsbPsPad->setChecked(config->readBoolEntry("UseUsbPsPad", false));
		chkRapidFire->setChecked(config->readBoolEntry("EnableRapidFire", false));
		chkGrabMouse->setChecked(config->readBoolEntry("GrabMouse", false));
	}
	
	if (loadCustom)
	{
		config->setGroup("xmame_options");
		customEditor->setText(config->readEntry("custom_switches", ""));
	}

	if (loadSdl)
	{
		config->setGroup("SDL");
		chkSdlFullscreen->setChecked(config->readBoolEntry("sdlFullscreen", false));
	
		QFile ffile( locateLocal("data", "kmameleon/") + "fsmodes.sdl" );
		kdDebug() << "Searching fsmodes" << endl;
		cbSdlFsModes -> clear();
		cbSdlFsModes->insertItem( i18n( "Default" ) );
		if ( ffile.open( IO_ReadOnly ) ) 
		{
			kdDebug() << "fsmodes file opened" << endl;
			QTextStream stream( &ffile );
			QString line;
			int i = 1;
			while ( !stream.atEnd() ) 
			{
				line = stream.readLine(); // line of text excluding '\n'
				i++;
				cbSdlFsModes -> insertItem(line);
			}
			ffile.close();
		}
		
		cbSdlFsModes -> setCurrentItem (config->readNumEntry("sdlFsMode", 0));
	}
	
	if (loadX11)
	{
		config->setGroup("X11");
		cbX11WindowMode -> setCurrentItem(config->readNumEntry("X11WindowMode", 0));
		cbX11ForceYuv -> setChecked(config->readBoolEntry("ForceYuv", false));
		cbX11ForceYv12 -> setChecked(config->readBoolEntry("ForceYv12",false));
		cbX11MitShm -> setChecked(config->readBoolEntry("MitShm",false));
		cbX11ShowCursor -> setChecked(config->readBoolEntry("ShowCursor",true));
		cbX11Widescreen -> setChecked(config->readBoolEntry("WideScreen",false));
		cbX11XSync -> setChecked(config->readBoolEntry("XSync",false));
	}

	modInfoReload();
}

void XMameCfg::slotOk()
{
	writeConfig();
	emit listenConfOk();
	close();
}

void XMameCfg::writeConfig()
{
	if (loadGeneral)
	{
		config->setGroup("general");
		config->writeEntry("xmame_rom",pthXmameRoms->text());
		config->writeEntry("xmame_samples",pthXmameSamples->text());
		config->writeEntry("xmame_art",pthXmameArt->text());
		config->writeEntry("xmame_screenshots",pthScreenShots->text());

		config->writeEntry("xmame_sdl",pthXmameSdlBin->text());
		config->writeEntry("xmame_x11",pthXmameX11Bin->text());
		config->writeEntry("xmame_gl",pthXmameGlBin->text());
		config->writeEntry("xmame_svga",pthXmameSvgaLibBin->text());

		config->writeEntry("showOutput",chkOutput->isChecked());
		config->writeEntry("color_debug",chkColorOutput->isChecked());
		config->writeEntry("showScreenshots", chkScreenShots->isChecked());
		config->writeEntry("xmame_type",xMameType->currentItem());
		config->writeEntry("setmode", cbConfigType->currentItem());
		config->writeEntry("cheatMode", chkCheatMode->isChecked());
		config->writeEntry("cheatFile", leCheatFile->text());
		config->writeEntry("custom", chkCustomSwitch->isChecked());
		config->writeEntry("restoreWindowPosition",chkRestoreWindowPos->isChecked());
		config->writeEntry("showList", chkShowListInstead->isChecked());

		config->writeEntry("detachProcess", chkDetachProcess->isChecked());
	}

	if (loadXGL)
	{
		config->setGroup("XGL");
		config->writeEntry("glTexSize",glleTexSize->text());
		config->writeEntry("glDGlLib",glleDGlLib->text());
		config->writeEntry("glDGluLib",glleDGluLib->text());
		config->writeEntry("glVBeamSize",glleVBeamSize->text());
		config->writeEntry("glcabModel",gllecabModel->text());
		config->writeEntry("glXYRes",glleXYRes->text());

		config->writeEntry("glFullscreen",glChkFullscreen->isChecked());
		config->writeEntry("glDblBuffer",glchkDblBuffer->isChecked());
		config->writeEntry("glBlit",glchkBlit->isChecked());
		config->writeEntry("glGlExt",glchkGlExt->isChecked());
		config->writeEntry("glBilinear",glchkBilinear->isChecked());
		config->writeEntry("glBitmap",glchkBitmap->isChecked());
		config->writeEntry("glVBitmap",glchkVBitmap->isChecked());
		config->writeEntry("glCMod",glchkCMod->isChecked());
		config->writeEntry("glAb",glchkAb->isChecked());
		config->writeEntry("glAA",glchkAA->isChecked());
		config->writeEntry("glVAA",glchkVAA->isChecked());
		config->writeEntry("glCabView",glchkCabView->isChecked());
	}

	if (loadAudio)
	{
		config->setGroup("audio");
		config->writeEntry("issound",chkIsSound->isChecked());
		config->writeEntry("samples",chkSamples->isChecked());
		config->writeEntry("fakesound",chkFakeSound->isChecked());
		config->writeEntry("volume",audioSetVolume->value());

		config->writeEntry("customsamplefrequency",chkCustomAudioSf->isChecked());
		config->writeEntry("custombufsize",chkCustomAudioBufSize->isChecked());
		config->writeEntry("customaudiodevice",chkCustomAudioDevice->isChecked());
		config->writeEntry("custommixerdevice",chkCustomAudioMixerDevice->isChecked());

		config->writeEntry("samplefrequency",audioSfEdit->text());
		config->writeEntry("bufsize",audioBufSizeEdit->text());
		config->writeEntry("audiodevice",audioDeviceEdit->text());
		config->writeEntry("mixerdevice",audioMixerDeviceEdit->text());
	}

	if (loadVideo)
	{
		config->setGroup("video");
		config->writeEntry("effect",videoFx->currentItem());
		config->writeEntry("maxautoframeskip",spnMaxAutoFrameSkip->value());
		config->writeEntry("noautoframeskip",spnNoAutoFrameSkip->value());
		config->writeEntry("enableframeskip",chkEnableFrameSkip->isChecked());
		config->writeEntry("enableautoframeskip",chkFrameSkip->isChecked());
		config->writeEntry("rotation",cbRotation->currentItem());
		config->writeEntry("flipx",chkFlipX->isChecked());
		config->writeEntry("flipy",chkFlipY->isChecked());
	}

	if (loadEffects)
	{
		config->setGroup("effects");
		config->writeEntry("enablescanlines",chkScanlines->isChecked());
		config->writeEntry("enableartwork",chkArtwork->isChecked());
		config->writeEntry("enableoverlays",chkOverlay->isChecked());
		config->writeEntry("enablebezel",chkBezel->isChecked());
		config->writeEntry("enableartcrop",chkArtCrop->isChecked());
		config->writeEntry("enablethrottle",chkThrottle->isChecked());
		config->writeEntry("enablesleepidle",chkSleepIdle->isChecked());
		config->writeEntry("brighteness",spnBrightness->value());
		config->writeEntry("fframes",spnFrames->value());
		config->writeEntry("xscale",spnXScale->value());
		config->writeEntry("yscale",spnYScale->value());
		config->writeEntry("scalevideo",spnScaleVideo->value());
		config->writeEntry("artres",spnArtResolution->value());
		config->writeEntry("gamma",edtGamma->text());
		config->writeEntry("aspectratio",edtAspectRatio->text());
	}

	if(loadDevices)
	{
		config->setGroup("devices");
		config->writeEntry("JoystickType",inputBox->currentItem());
		config->writeEntry("AnalogStick", chkAnalogStick->isChecked());
		config->writeEntry("JoyDevice", customJoyDevName->text());
		config->writeEntry("UseHotRod", chkUseHotRod->isChecked());
		config->writeEntry("UseHotRodSe", chkUseHotRodSe->isChecked());
		config->writeEntry("UseUsbPsPad", chkUsbPsPad->isChecked());
		config->writeEntry("EnableRapidFire", chkRapidFire->isChecked());
		config->writeEntry("GrabMouse", chkGrabMouse->isChecked());
	}
	
	if (loadCustom)
	{
		config->setGroup("xmame_options");
		config->writeEntry("custom_switches", customEditor->text());
	}
	
	if (loadSdl)
	{
		config->setGroup("SDL");
		config->writeEntry("sdlFullscreen", chkSdlFullscreen->isChecked());
		config->writeEntry("sdlFsMode", cbSdlFsModes -> currentItem ());
	}
	
	if (loadX11)
	{
		config->setGroup("X11");
		config->writeEntry("X11WindowMode",cbX11WindowMode -> currentItem());
		config->writeEntry("ForceYuv",cbX11ForceYuv -> isChecked());
		config->writeEntry("ForceYv12",cbX11ForceYv12 -> isChecked());
		config->writeEntry("MitShm",cbX11MitShm -> isChecked());
		config->writeEntry("ShowCursor",cbX11ShowCursor -> isChecked());
		config->writeEntry("WideScreen",cbX11Widescreen -> isChecked());
		config->writeEntry("XSync",cbX11XSync -> isChecked());
	}

//saving the current options...
	saveOptions(true);
}

void XMameCfg::saveOptions(bool writing)
{
	QString temp;
	int xmMode=1;
	if (xmModes(0) != 0)
		xmMode = xmModes(0);
	if (! xmModes(1) != 0)
		xmMode = xmModes(1);
	if (! xmModes(2) != 0)
		xmMode = xmModes(2);
	if (! xmModes(3) != 0)
		xmMode = xmModes(3);

//cheat options...
	if(chkCheatMode->isChecked() && !leCheatFile->text().isEmpty())
		loadedOptions += " -cheat  -cheat_file " + leCheatFile->text();

//video
	loadedOptions += " -effect " + temp.setNum(videoFx->currentItem()) + " ";

	switch (cbRotation->currentItem())
	{
		case 1:
			loadedOptions += " -norotate ";
		break;
		case 2:
			loadedOptions += " -rol ";
		break;
		case 3:
			loadedOptions += " -ror ";
		break;
	}

	if (chkEnableFrameSkip->isChecked())
	{
		if (chkFrameSkip->isChecked())
			loadedOptions += " -autoframeskip ";
		else
			loadedOptions += " -noautoframeskip ";
	
		loadedOptions += " -maxautoframeskip " + temp.setNum(spnMaxAutoFrameSkip->value());
		loadedOptions += " -frameskip " + temp.setNum(spnNoAutoFrameSkip->value());
	}
	
	if (chkFlipX->isChecked())
		loadedOptions += " -flipx ";

	if (chkFlipY->isChecked())
		loadedOptions += " -flipy ";

//screen
	switch (xMameType->currentItem())
	{
		case 1:
			if (chkSdlFullscreen -> isChecked())
				loadedOptions +=" -fullscreen ";
		break;
		
		case 2:
			//pac loadedOptions +=" -x11 " + QString::number(cbX11WindowMode->currentItem());
			if(cbX11WindowMode->currentItem()==2)
				loadedOptions +=" -vidmod 1 -fullscreen ";
			else
				loadedOptions +=" -vidmod " + QString::number(cbX11WindowMode->currentItem());
		break;

		case 3:
			if (glChkFullscreen -> isChecked())
				loadedOptions +=" -fullscreen ";
		break;
	}

//ROM path...
	if (writing)
		loadedOptions += " -rompath " + pthXmameRoms->text();

//audio
// obsolete
// 	if (! chkIsSound->isChecked())
// 		loadedOptions += " -nosound ";
	if (! chkSamples->isChecked())
		loadedOptions += " -nosamples ";
// obsolete
// 	if (chkFakeSound->isChecked())
// 		loadedOptions += " -fakesound ";

	loadedOptions += " -volume " + QString::number(audioSetVolume->value())+" ";

	if(chkCustomAudioSf->isChecked() && audioSfEdit->text() !="")
		loadedOptions += " -samplefreq " + audioSfEdit->text();
	if(chkCustomAudioBufSize->isChecked() && audioBufSizeEdit->text() !="")
		loadedOptions += " -bufsize " +audioBufSizeEdit->text();
	if(chkCustomAudioDevice->isChecked() && audioDeviceEdit->text() !="")
		loadedOptions += " -audiodevice "+audioDeviceEdit->text();
	if(chkCustomAudioMixerDevice->isChecked() && audioMixerDeviceEdit->text() !="")
		loadedOptions += " -mixerdevice " +audioMixerDeviceEdit->text();

//input devices
	loadedOptions += " -joytype " + QString::number(inputBox->currentItem());
	if (customJoyDevName->text() !="")
		loadedOptions += " -jdev " + customJoyDevName->text();

	if (chkAnalogStick->isChecked())
		loadedOptions += " -analogstick ";
	if (chkUseHotRod->isChecked())
		loadedOptions += " -hotrod ";
	if (chkUseHotRodSe->isChecked())
		loadedOptions += " -hotrodse ";
	if (chkUsbPsPad->isChecked())
		loadedOptions += " -usbpspad ";
	if (chkRapidFire->isChecked())
		loadedOptions += " -rapidfire ";

//these are advanced options -- disabled in simple mode
	if (cbConfigType->currentItem() == 1)
	{
		if (chkScanlines->isChecked())
			loadedOptions += " -scanlines ";

		if (chkArtwork->isChecked())
			loadedOptions += " -artwork ";

		if (chkBackdrops->isChecked())
			loadedOptions += " -backdrop ";

		if (chkOverlay->isChecked())
			loadedOptions += " -overlay ";

		if (chkBezel->isChecked())
			loadedOptions += " -bezel ";

		if (chkArtCrop->isChecked())
			loadedOptions += " -artcrop ";

		if (chkThrottle->isChecked())
			loadedOptions += " -th ";

		if (chkSleepIdle->isChecked())
			loadedOptions += " -si ";

		if (spnBrightness->value() > 0)
			loadedOptions += " -brt " + QString::number(spnBrightness->value());

		if (spnFrames->value() > 0)
			loadedOptions += " -ftr " + QString::number(spnFrames->value());

		if (spnXScale->value() > 0)
			loadedOptions += " -hs " + QString::number(spnXScale->value());

		if (spnYScale->value() > 0)
			loadedOptions += " -ws " + QString::number(spnXScale->value());

		if (spnScaleVideo->value() > 0)
			loadedOptions += " -ah " + QString::number(spnScaleVideo->value());

		if (spnArtResolution->value() > 0)
			loadedOptions += " -artres " + QString::number(spnArtResolution->value());

		if (edtGamma->text() !="")
			loadedOptions += " -gc " + edtGamma->text();

		if (edtAspectRatio->text() !="")
			loadedOptions += " -scale " + edtAspectRatio->text();
	}
	
	if (xmMode == 3 || xMameType -> currentItem() == 3)
	{
		if (! glleTexSize -> text().isEmpty())
			loadedOptions += " -gltexture_size " + glleTexSize -> text();
	
		if (! glleDGlLib -> text().isEmpty())
			loadedOptions += " -gllibname " + glleDGlLib -> text();
	
		if (! glleDGluLib -> text().isEmpty())
			loadedOptions += " -glulibname " + glleDGluLib -> text();
	
		if (! glleVBeamSize -> text().isEmpty())
			loadedOptions += " -glbeam " + glleVBeamSize -> text();
	
		if (! gllecabModel -> text().isEmpty())
			loadedOptions += " -cabinet " + gllecabModel -> text();
	
		if (! glleXYRes -> text().isEmpty())
			loadedOptions += " -glres " + glleXYRes -> text();
	
		if (! glchkDblBuffer -> isChecked())
			loadedOptions += " -nogldblbuffer ";
	
		if (! glchkBlit -> isChecked())
			loadedOptions += " -noglforceblitmode ";
	
		if (! glchkGlExt -> isChecked())
			loadedOptions += " -noglext78 ";
	
		if (! glchkBilinear -> isChecked())
			loadedOptions += " -noglbilinear ";
	
		if (! glchkBitmap -> isChecked())
			loadedOptions += " -noglbitmap ";
	
		if (! glchkVBitmap -> isChecked())
			loadedOptions += " -noglbitmapv ";
	
		if (! glchkCMod -> isChecked())
			loadedOptions += " -noglcolormod ";
	
		if (! glchkAb -> isChecked())
			loadedOptions += " -noglalphablending ";
	
		if (! glchkAA -> isChecked())
			loadedOptions += " -noglaa ";
	
		if (! glchkVAA -> isChecked())
			loadedOptions += " -noglaav ";
	
		if (glchkCabView -> isChecked())
			loadedOptions += " -cabview ";
	
		if (chkGrabMouse->isChecked())
			loadedOptions += " -grabmouse ";
	}
	
	if (xmMode == 2 || xMameType -> currentItem() == 2)
	{
// obsolete
// 		if (cbX11ForceYuv -> isChecked())
// 			loadedOptions += " -yuv ";
		
// obsolete
// 		if (cbX11ForceYv12 -> isChecked())
// 			loadedOptions += " -yv12 ";
		
		if (cbX11MitShm -> isChecked())
			loadedOptions += " -mitshm ";
		
		if (cbX11ShowCursor -> isChecked())
			loadedOptions += " -cursor ";
		else
			loadedOptions += " -nocursor ";
// obsolete
// 		if (cbX11Widescreen -> isChecked())
// 			loadedOptions += " -widescreen ";
		
		if (cbX11XSync -> isChecked())
			loadedOptions += " -xsync ";
		
		if (chkGrabMouse->isChecked())
			loadedOptions += " -grabmouse ";
	}
	
	if (xmMode == 1 || xMameType -> currentItem() == 1)
	{
		int sdlFsMode = cbSdlFsModes -> currentItem();
		if (sdlFsMode > 0) 
			loadedOptions += "  -modenumber " + QString::number(sdlFsMode -1);
	}
	
//writing current options...
	if (writing)
	{
		config->setGroup("xmame_options");
		config->writeEntry("current_options", loadedOptions);
	}
	else
		customEditor->setText(loadedOptions);
		
	kdDebug() << loadedOptions << endl;

	loadedOptions = "";
}

void XMameCfg::setDefaultPaths(KProcess *, char *text, int len)
{
	kdDebug() << "setDefaultPaths() running" << endl;
	QStringList list;

	QString temp;
	int pos;
	QString dataStream;

	temp.fill(' ',len+1);
	temp.replace(0,len,text);
	temp[len]='\0';
	paths=QStringList::split("\n", temp.local8Bit());

	for ( unsigned int i= 0; i < paths.count(); i++)
	{
		if (paths[i].contains("rompath") > 0 && paths[i].contains("/") > 0)
		{
			kdDebug() << paths[i] << endl;
			list += paths[i];
		}
		if (paths[i].contains("samplepath") > 0 && paths[i].contains("/") > 0)
		{
			kdDebug() << paths[i] << endl;
			list += paths[i];
		}
		if (paths[i].contains("artwork_directory") > 0 && paths[i].contains("/") > 0)
		{
			kdDebug() << paths[i] << endl;
			list += paths[i];
		}
		if (paths[i].contains("snapshot_directory") > 0 && paths[i].contains("/") > 0)
		{
			kdDebug() << paths[i] << endl;
			list += paths[i];
		}
	}

	//for ( unsigned int i= 0; i<list.count(); i++)
	//	kdDebug() << list[i] << endl;

//this will handle the paths...
	config->setGroup("general");
	for (int pthEnv =0; pthEnv <=3; pthEnv++)
	{
		switch(pthEnv)
		{
			case 0: //rompath
				pos = list[pthEnv].find("/");
				dataStream = list[pthEnv].mid(pos);
				//kdDebug() << dataStream << endl;
				if (config->readEntry("xmame_rom","").isEmpty())
				{
					pthXmameRoms->setText(dataStream);
					kdDebug() << dataStream << endl;
					//config->writeEntry("xmame_rom",dataStream);
				}
				else
					if (KMessageBox::questionYesNo(this,i18n("This will override Your current rom path settings... Want me to proceed?")) == 3)
					{
						pthXmameRoms->setText(dataStream);
						kdDebug() << dataStream << endl;
					}
			break;
			case 1: //samplepath
				pos = list[pthEnv].find("/");
				dataStream = list[pthEnv].mid(pos);
				//kdDebug() << dataStream << endl;
				if (config->readEntry("xmame_samples","").isEmpty())
				{
					pthXmameSamples->setText(dataStream);
					kdDebug() << dataStream << endl;
					//config->writeEntry("xmame_samples",dataStream);
				}
				else
					if (KMessageBox::questionYesNo(this,i18n("This will override Your current sample path settings... Want me to proceed?")) == 3)
					{
						pthXmameSamples->setText(dataStream);
						kdDebug() << dataStream << endl;
					}
			break;
			case 2: //artworkpath
				pos = list[pthEnv].find("/");
				dataStream = list[pthEnv].mid(pos);
				//kdDebug() << dataStream << endl;
				if (config->readEntry("xmame_art","").isEmpty())
				{
					pthXmameArt->setText(dataStream);
					kdDebug() << dataStream << endl;
					//config->writeEntry("xmame_art",dataStream);
				}
				else
					if (KMessageBox::questionYesNo(this,i18n("This will override Your current artwork path settings... Want me to proceed?")) == 3)
					{
						pthXmameArt->setText(dataStream);
						kdDebug() << dataStream << endl;
					}
			break;
			case 3: // screenshots
				pos = list[pthEnv].find("/");
				dataStream = list[pthEnv].mid(pos);
				//kdDebug() << dataStream << endl;
				if (config->readEntry("xmame_screenshots","").isEmpty())
				{
					pthScreenShots->setText(dataStream);
					kdDebug() << dataStream << endl;
					//config->writeEntry("xmame_screenshots",dataStream);
				}
				else
					if (KMessageBox::questionYesNo(this,i18n("This will override Your current screenshots path settings... Want me to proceed?")) == 3)
					{
						pthScreenShots->setText(dataStream);
						kdDebug() << dataStream << endl;
					}
			break;
		} //end switch
	} // end for loop


//a warning...
	if (pthXmameRoms->text().isEmpty())
		KMessageBox::error(this, i18n("KMamelon can't find the path to XMame roms... \nPlease configure it..."));

	//config->sync();
	writeConfig();
	emit listenConfOk();
	
	list = 0L;
	
	adProc.closeStdout();
	adProc.kill();
}

void XMameCfg::changeVolumeLevel(int lvlChanged)
{
	lblDisplayVolume->setText(QString::number(lvlChanged) +i18n(" dB"));
}

void XMameCfg::slotConfigType(int cfgLevel)
{
	kdDebug() << cfgLevel << endl;
}

void XMameCfg::setVersionInfo(KProcess *, char *text, int len)
{
	//QString temp;
	QString prevVersion;
	bool hasChanged=false;

	kdDebug() << "VersionInfo executed!" << endl;
		
	versionBrowser -> append("\n");
	if (xMameType -> currentItem() == 0)
		versionBrowser -> append(i18n("Autodetected:"));
	else
		versionBrowser -> append(i18n("Using:"));

	//temp.fill(' ',len);
	//temp.replace(0,len-2,text);
	//temp[len-1]='';
	text[len-1]='\0';
	
	config->setGroup("general");
	prevVersion=config->readEntry("xmameVersion", "");
	
	config->setGroup("First_run");
	if((prevVersion != text) && !config->readBoolEntry("First",true)) {
		hasChanged=true; }

	config->setGroup("general");
	config->writeEntry("xmameVersionHasChanged", hasChanged);
	config->writeEntry("xmameVersion", text);
	config->sync();

	//versionBrowser -> append(temp.local8Bit());
	versionBrowser -> append(text);
	versionBrowser -> append("\n");

	nfoProc.closeStdout();
	nfoProc.kill();
}

void XMameCfg::slotSetCheatFile()
{
	QString cheatFile = KFileDialog::getOpenFileName(QString::null, QString::null, this, i18n("Select cheat file"));
	if (! cheatFile.isEmpty())
	{
		leCheatFile->setText(cheatFile);
	}
}

void XMameCfg::loadCurrentParameters()
{
	saveOptions(false);
}

void XMameCfg::slotGetSdlFsModes()
{
	if (pthXmameSdlBin -> text().isEmpty())
	{
		KMessageBox::sorry(this,i18n("You don't have SDL version of XMame configured"));
		return;
	}
	
	cbSdlFsModes->clear();
	cbSdlFsModes->insertItem( i18n( "Default" ) );
	
	QString options;
	options = pthXmameSdlBin -> text();
	options +=" -listmodes ";

	sdlListmodesProc.clearArguments();
	#if KDE_VERSION >= 306
		sdlListmodesProc.setUseShell(true);
	#endif

	sdlListmodesProc << options.local8Bit();
	if (! sdlListmodesProc.start(KProcess::NotifyOnExit, KProcess::AllOutput) )
		kdDebug() << "Canot execute SDL mode listing" << endl;
}

void XMameCfg::loadSdlFsModes(KProcess *, char *text, int len)
{
	kdDebug() << "SDL Fullscreen modes:" << endl;
	
	QString allItems = "";
	QString temp;
	
	temp.fill(' ',len+1);
	temp.replace(0,len,text);
	temp[len]='\0';
	QStringList modeItems = QStringList::split("\n",temp.utf8());
	QStringList modeItem;
	for (unsigned int x = 0; x < modeItems.size(); x++)
	{
		modeItem = QStringList::split(")", modeItems[x]); //will split: number / mode description
		if (! modeItem[1].stripWhiteSpace().isEmpty())
		{
			kdDebug() << modeItem[1] << endl;
			cbSdlFsModes->insertItem( i18n( modeItem[1].stripWhiteSpace() ) );
			allItems += modeItem[1].stripWhiteSpace() + "\n";
		}
		modeItem.clear();
	}
	
	QFile f(locateLocal("data", "kmameleon/") + "fsmodes.sdl");
	f.open( IO_WriteOnly);
	f.writeBlock( allItems, qstrlen(allItems) );      // write to stderr
	f.close();
	
	temp.fill(' ',len+1);
	temp.replace(0,len,text);
	temp[len]='\0';
	kdDebug() << allItems << endl;
}

int XMameCfg::xmModes(int mode)
{
	QString conf_xmameBinPath;
	int ret_nr = 0;
	switch (mode)
	{
		case 0:
			conf_xmameBinPath=config->readEntry("xmame_sdl","");
			if(! conf_xmameBinPath.isEmpty())
				ret_nr = 1;
		break;

		case 1:
			conf_xmameBinPath=config->readEntry("xmame_x11","");
			if(! conf_xmameBinPath.isEmpty())
				ret_nr = 2;
		break;

		case 2:
			conf_xmameBinPath=config->readEntry("xmame_gl","");
			if(! conf_xmameBinPath.isEmpty())
				ret_nr = 3;
		break;

		case 3:
			conf_xmameBinPath=config->readEntry("xmame_svga","");
			if(! conf_xmameBinPath.isEmpty())
				ret_nr = 4;
		break;
	}
	return ret_nr;
}


#include "xmameconfig.moc"
