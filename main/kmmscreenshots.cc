/***************************************************************************
                          kmmscreenshots.cc  -  description
                             -------------------
    copyright            : (C) 2002 by Whitehawk Stormchaser
    email                : zerokode@gmx.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#include "kmmscreenshots.h"

ScreenShotWindow::ScreenShotWindow( QWidget *parent, const char *name, WFlags f )
    : DlgScreenShots( parent, name, f )
{
}

ScreenShotWindow::~ScreenShotWindow()
{
}

void ScreenShotWindow::closeEvent(QCloseEvent *)
{

}

#include "kmmscreenshots.moc"
