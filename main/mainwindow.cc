/***************************************************************************
                          mainwindow.cc  -  description
                             -------------------
    copyright            : (C) 2002 by Whitehawk Stormchaser
    email                : zerokode@gmx.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#include "mainwindow.h"
#include <qpixmap.h>
#include <qthread.h>

KMameleon::KMameleon()
{
//inits...
	bool typeOfBuild=false;
	config = kapp->config();
	
	QPixmap pixmap( QPixmap(locate("data","kmameleon/pics/splash.png")) );
	QSplashScreen *splash = new QSplashScreen( pixmap );
	splash->show();
	splash->QWidget::setCursor( QCursor(Qt::WaitCursor) );
	splash->message( "Loading..." , Qt::AlignBottom);

	newConfig = new XMameCfg(this);
	
	helpMenu = new KHelpMenu(this, KGlobal::instance()->aboutData(), true);
	mnuPopup = new KPopupMenu(this);
	toolPopup = new KPopupMenu(this);
	loadPopups();
	
	dbgWindow = new DevDebug( 0, "dlg_debug");
	ssWindow = new ScreenShotWindow( 0, "dlg_screenshots");
 
	glWindow = new ItemGamesList(this,"GameList",Qt::WType_Dialog); //games list window
	glWindow->splash=splash;
	
	// detect if xmame version has changed
	config->setGroup("general");
	if(config->readBoolEntry("xmameVersionHasChanged", false)) {
		if (KMessageBox::questionYesNo(this,i18n("Xmame version seems to have changed. Do you want me to rescan ROMS ?")) == KMessageBox::Yes)
		typeOfBuild=true;
	}
	
	glWindow->buildLists(typeOfBuild);

	options = "";

//button pixmaps
	setFixedSize(450, 55);
	setCaption(i18n("KMameleon %1").arg(__kmmversion__));
	btnCfg->setPixmap(QPixmap(locate("data","kmameleon/pics/conf.png")) );
	btnPlay->setPixmap(QPixmap(locate("data","kmameleon/pics/play.png")) );
	btnQuit->setPixmap(QPixmap(locate("data","kmameleon/pics/off.png")) );
	btnHelp->setPixmap(QPixmap(locate("data","kmameleon/pics/help.png")) );
	btnStop->setPixmap(QPixmap(locate("data","kmameleon/pics/stop.png")) );
	btnReload->setPixmap(QPixmap(locate("data","kmameleon/pics/reload.png")) );
	btnTools->setPixmap(QPixmap(locate("data","kmameleon/pics/tools.png")) );

//connections
	connect (btnQuit, SIGNAL(clicked()), this, SLOT(close()) );
	connect (btnPlay, SIGNAL(clicked()), this, SLOT(runPlay()) );
	connect (btnStop, SIGNAL(clicked()), this, SLOT(goStop()) );
	connect (btnCfg, SIGNAL(clicked()), this, SLOT(runConf()) );
	connect (newConfig, SIGNAL(listenConfOk()), this, SLOT(reloadList()) );
	connect (btnHelp, SIGNAL(clicked()), this, SLOT(popHelp()) );
	connect (btnTools, SIGNAL(clicked()), this, SLOT(popTools()) );
	connect (btnReload, SIGNAL(clicked()), this, SLOT(reload()) );
	connect (&proc, SIGNAL( receivedStderr(KProcess *, char *, int)), this, SLOT( getErrOutput( KProcess *, char *, int)) );
	connect (&proc, SIGNAL( receivedStdout(KProcess *, char *, int)), this, SLOT( getOutput( KProcess *, char *, int)) );
	connect (&proc, SIGNAL( processExited (KProcess *)), this, SLOT( getStoppedStatus(KProcess *)) );

//tooltips
	QToolTip::add(btnQuit, i18n("Exit KMameleon"));
	QToolTip::add(btnPlay, i18n("Let\'s play games!"));
	QToolTip::add(btnCfg, i18n("Configure KMameleon"));
	QToolTip::add(btnStop, i18n("Stop currently running xmame"));
	QToolTip::add(btnHelp, i18n("Help"));
	QToolTip::add(btnReload, i18n("Reload Gamelist"));
	QToolTip::add(btnTools, i18n("KMameleon tools"));
	
//loading directory list, if the ROM path is defined...
	reloadList();

//locking the "stop" button
	btnStop->setEnabled(false);

//loading the tools...
	romChecker = new ToolRomCheckerPlugin();
	pathFinder = new ToolPathFinderPlugin();

//load the image of the file
	loadScreenShots();

	splash->finish( this );
	splash->QWidget::setCursor( QCursor(Qt::ArrowCursor) );
	splash->hide();
	this->show();
}

KMameleon::~KMameleon()
{
}

QString KMameleon::modes(int mode)
{
	bool conf_isFullscreen;
	config->setGroup("general");
	switch (mode)
	{
		case 0:
			conf_xmameBinPath=config->readEntry("xmame_sdl","");
			if(! conf_xmameBinPath.isEmpty())
			{
				kdDebug() << "SDL" << endl;
				config->setGroup("SDL");
				conf_isFullscreen=config->readBoolEntry("sdlFullscreen",false);
				if (conf_isFullscreen)
				{
					fsOptions += "  -fullscreen ";
					kdDebug() << "SDL fullscreen" << endl;
				}
			}
		break;

		case 1:
			conf_xmameBinPath=config->readEntry("xmame_x11","");
			if(! conf_xmameBinPath.isEmpty())
			{
				kdDebug() << "X11" << endl;
				config->setGroup("X11");
				conf_isFullscreen=config->readBoolEntry("x11Fullscreen",false);
				if (conf_isFullscreen)
				{
					fsOptions += "  -fullscreen";
					kdDebug() << "X11 fullscreen" << endl;
				}
			}
		break;

		case 2:
			conf_xmameBinPath=config->readEntry("xmame_gl","");
			if(! conf_xmameBinPath.isEmpty())
			{
				kdDebug() << "XGL" << endl;
				config->setGroup("XGL");
				conf_isFullscreen=config->readBoolEntry("glFullscreen",false);
				if (conf_isFullscreen) 
				{
					fsOptions += "  -fullscreen";
					kdDebug() << "XGL fullscreen" << endl;
				}
			}
		break;

		case 3:
			conf_xmameBinPath=config->readEntry("xmame_svga","");
			if(! conf_xmameBinPath.isEmpty())
			{
				kdDebug() << "SVGALIB" << endl;
				config->setGroup("screen");
				conf_isFullscreen=config->readBoolEntry("fullscreen_svga",false);
				if (conf_isFullscreen)
				{
					fsOptions += "  -fullscreen";
					kdDebug() << "SVGALIB fullscreen" << endl;
				}
			}
		break;
	}
	return conf_xmameBinPath;
}

void KMameleon::runPlay()
{
	int xmame_type;

	dbgWindow->dbgOut->setText("");

	config->setGroup("general");
	xmame_type=config->readNumEntry("xmame_type",0);

	switch (xmame_type)
	{
		case 0:
			if (! modes(0).isEmpty()) {
				break;
			}
			if (! modes(1).isEmpty()) {
				break;
			}
			if (! modes(2).isEmpty()) {
				break;
			}
			if (! modes(3).isEmpty()) {
				break;
			}
		break;

		case 1:
			config->setGroup("general");
			conf_xmameBinPath=config->readEntry("xmame_sdl","");
		break;

		case 2:
			config->setGroup("general");
			conf_xmameBinPath=config->readEntry("xmame_x11","");
		break;

		case 3:
			config->setGroup("general");
			conf_xmameBinPath=config->readEntry("xmame_gl","");
		break;
	}

	config->setGroup(i18n("general"));
	bool fromGameList = config->readBoolEntry("showList",false);
	
	QListViewItem *item = glWindow->getSelect();
	
	if(fromGameList && (QListViewItem *)item==NULL) {
		KMessageBox::error( 0, i18n("No rom selected !") );
		return;
	}
	
	if(fromGameList) {
		conf_selectedGame = item->text(1);
	} else {
		conf_selectedGame = lstGames->currentText();
	}
	proc.clearArguments();
	#if KDE_VERSION >= 306
		proc.setUseShell(true);
	#endif

	bool switchCustom;
	config->setGroup("general");
	if (config->readBoolEntry("custom", false)) {
		switchCustom = true;
	} else {
		switchCustom = false;
	}
	
	QString romPath( config->readEntry("xmame_rom", "") );
			
	if (! conf_selectedGame.isEmpty())
	{ 
		if (! conf_xmameBinPath.isEmpty())
		{
			config->setGroup("xmame_options");
			if (switchCustom)
			{
				options=config->readEntry("custom_switches", "");
				options +=" -rompath " + romPath;
			}
			else { 
				options=config->readEntry("current_options", "");
			//kdDebug() << options << fsOptions << endl;
			}

			proc << conf_xmameBinPath << options.local8Bit() << fsOptions << conf_selectedGame;
                        config -> setGroup("general");
			config -> writeEntry("last_played", conf_selectedGame);

			if (! proc.start(KProcess::NotifyOnExit, KProcess::AllOutput)) {
				KMessageBox::information( 0, i18n("Couldn't load a program") );
			}

			if (proc.isRunning()) {
				btnStop->setEnabled(true);
			}
		}
		else {
			KMessageBox::error( 0, i18n("XMame binary path seems to be missing") );
		}

		kdDebug() << xmame_type << endl;
		kdDebug() << conf_xmameBinPath << endl;
		kdDebug() << options << endl;
		options="";
	}
	else {
		
		KMessageBox::error( 0, i18n("No rom selected !") );
	}
}

void KMameleon::runConf()
{
//	newConfig -> loadConfig();
	saveWindowsPos();
	newConfig -> show();
}


void KMameleon::reload()
{
	glWindow->buildLists(true);
	reloadList();
}

void KMameleon::reloadList()
{	
	QStringList gamesList(glWindow->getPresent()); // fill gamesList with games present
	bool found=false;
	unsigned int countItem=0;
	unsigned int numItem=gamesList.count();
	
	lstGames->clear();
	lstGames->insertStringList(gamesList);

	config -> setGroup("general");
	QString lastPlayed = config -> readEntry("last_played","");
	while (! found && countItem<numItem) {
		if (lstGames -> text(countItem) == lastPlayed) {
			lstGames -> setCurrentItem(countItem);
			found=true;
		}
		countItem++;
	}
		
	if (config->readBoolEntry("showList", true)) 
	{
		btnPlay -> setUsesBigPixmap(true);
		glWindow -> show();
		lstGames -> hide();
	} 
	else 
	{
		btnPlay -> setUsesBigPixmap(false);
		glWindow -> hide();
		lstGames -> show();
	}
	//}

//we'll check, wether it is up to show the debug window, too...
	config->setGroup("general");
	if (config->readBoolEntry("showOutput",true)) {
		dbgWindow->show();
	} else {
		dbgWindow->hide();
	}

//Show me the screenshots window...
	if (config->readBoolEntry("showScreenshots",true) && !config->readEntry("xmame_screenshots", "").isEmpty()) {
		ssWindow->show();
	} else {
		ssWindow->hide();
	}

//windows pos
	if (config->readBoolEntry("restoreWindowPosition"))
	{
		this->move(config->readNumEntry("posXMainWindow",0), config->readNumEntry("posYMainWindow",0));
		dbgWindow->move(config->readNumEntry("posXDebugWindow",0), config->readNumEntry("posYDebugWindow",0));
		ssWindow->move(config->readNumEntry("posXScreenShotWindow",0),config->readNumEntry("posYScreenShotWindow",0));
		glWindow->move(config->readNumEntry("posXGameListWindow",0),config->readNumEntry("posYGameListWindow",0));
		glWindow->resize(config->readNumEntry("widthGameListWindow", 0),config->readNumEntry("heightGameListWindow",0));
		
		kdDebug() << "Restoring window positions: this->x(): " << config->readNumEntry("posXMainWindow",0) << " | ssWindow->y(): " << config->readNumEntry("posYMainWindow",0) << endl;
		kdDebug() << "Restoring window positions: dbgWindow->x(): " << config->readNumEntry("posXDebugWindow",0) << " | dbgWindow->y(): " << config->readNumEntry("posYDebugWindow",0) << endl;
		kdDebug() << "Restoring window positions: ssWindow->x(): " << config->readNumEntry("posXScreenShotWindow",0) << " | ssWindow->y(): " << config->readNumEntry("posYScreenShotWindow",0) << endl;
	}
}

void KMameleon::loadPopups()
{
//help menu...
	mnuPopup = helpMenu->menu();

//Tools & gadgets...
	toolPopup->setTitle(i18n("Tools"));
	toolPopup->insertItem(QPixmap(locate("data","kmameleon/tools/img_pathfinder.png")), i18n("Path finder"), this, SLOT(loadPathFinder()));
	toolPopup->insertItem(QPixmap(locate("data","kmameleon/tools/img_romcheck.png")), i18n("Rom checker"), this, SLOT(loadRomChecker()));
}

void KMameleon::popHelp()
{
	mnuPopup->exec(QCursor::pos());
}

void KMameleon::popTools()
{
	toolPopup->exec(QCursor::pos());
}

void KMameleon::getOutput(KProcess *, char *text, int len)
{
	temp.fill(' ',len+1);
	temp.replace(0,len,text);
	temp[len]='\0';

//colorful output...
	QString dta="\0";
	dta = "<font color=\"red\">" + temp.replace("\n","<br />\n") + "</font><br>";
//	kdDebug() << temp.local8Bit() << endl; //debug to console...
	
	config->setGroup("general");
	if (config->readBoolEntry("color_debug",true)) {
		dbgWindow->dbgOut->append(dta.local8Bit());
	} else {
		dbgWindow->dbgOut->append(temp.local8Bit());
	}
//	kdDebug() << dbgWindow->dbgOut->text() << endl;

}

void KMameleon::getErrOutput(KProcess *, char *text, int len)
{
	temp.fill(' ',len+1);
	temp.replace(0,len,text);
	temp[len]='\0';
	
//rather write output in the debug window as the kdDebug()
	dbgWindow->dbgOut->append(temp.local8Bit());	
}

void KMameleon::goStop()
{
	if (! proc.kill()) {
		KMessageBox::error(this,i18n("Can't tell current XMame session to stop!"));
	}
}

void KMameleon::getStoppedStatus(KProcess *proc)
{
	btnStop->setEnabled(false);
	if(! proc->normalExit ()) {
		KMessageBox::error(this,i18n("Xmame abnormal termination !"));
	} else {
		if(proc -> exitStatus() != 0 &&  proc -> exitStatus() != 1) {
			KMessageBox::error(this,i18n("Can't run the ROM !"));
		}
	}
}

//from here on... The tools are loaded [NO exceptions!!!]
void KMameleon::loadRomChecker()
{
//	pathFinder->hide();
	romChecker->show();
}

void KMameleon::loadPathFinder()
{
//	romChecker->hide();
	pathFinder->show();
}

void KMameleon::closeEvent(QCloseEvent *)
{
	config->setGroup("general");
	if(! config->readBoolEntry("detachProcess", false)) {
		proc.kill();
	}
	saveWindowsPos();
	kapp->exit();
}

void KMameleon::loadScreenShots()
{
	config->setGroup("general");
	if (config->readBoolEntry("showScreenshots",true) && !config->readEntry("xmame_screenshots", "").isEmpty())
	{
		QString imagePath = config->readEntry("xmame_screenshots", "") + "/" + lstGames -> currentText() + ".jpg";
		QFileInfo fi(imagePath);
		if (fi.exists())
		{
			kdDebug() << "Found picture: " << imagePath << ". Loaded." << endl;
			QPixmap pix = QPixmap(imagePath);
			ssWindow->setMinimumWidth(pix.width());
			ssWindow->setMinimumHeight(pix.height());
			ssWindow->setMaximumWidth(pix.width());
			ssWindow->setMaximumHeight(pix.height());
			ssWindow->resize(pix.width(), pix.height());
			ssWindow->setPaletteBackgroundPixmap(pix);
		}
		else
		{
			kdDebug() << "Missing picture: " << imagePath << ". Loaded " << locate("data","kmameleon/pics/noss.png") << " instead." << endl;
			QPixmap pix = QPixmap(locate("data","kmameleon/pics/noss.png"));
			ssWindow->setMinimumWidth(pix.width());
			ssWindow->setMinimumHeight(pix.height());
			ssWindow->setMaximumWidth(pix.width());
			ssWindow->setMaximumHeight(pix.height());
			ssWindow->resize(pix.width(), pix.height());
			ssWindow->setPaletteBackgroundPixmap(pix);
		}
	}
}

void KMameleon::keyPressEvent( QKeyEvent *k )
{
    switch ( k->key() ) {
        case Qt::Key_Return:
		runPlay();
            break;
	   
	case Qt::Key_F12:
		close();
            break;	
	   
	case Qt::Key_F11:
		runConf();
            break;	
    }
}

void KMameleon::saveWindowsPos(void) {
//Save window positions:
	config->setGroup("general");
	config->writeEntry("posXMainWindow",this->x());
	config->writeEntry("posYMainWindow",this->y());
	config->writeEntry("widthMainWindow",this->width());
	config->writeEntry("heightMainWindow",this->height());
	
	config->writeEntry("posXDebugWindow",dbgWindow->x());
	config->writeEntry("posYDebugWindow",dbgWindow->y());
	config->writeEntry("widthDebugWindow",dbgWindow->width());
	config->writeEntry("heightDebugWindow",dbgWindow->height());
	
	config->writeEntry("posXScreenShotWindow",ssWindow->x());
	config->writeEntry("posYScreenShotWindow",ssWindow->y());
	config->writeEntry("widthScreenShotWindow",ssWindow->width());
	config->writeEntry("heightScreenShotWindow",ssWindow->height());
	
	config->writeEntry("posXGameListWindow",glWindow->x());
	config->writeEntry("posYGameListWindow",glWindow->y());
	config->writeEntry("widthGameListWindow",glWindow->width());
	config->writeEntry("heightGameListWindow",glWindow->height());
	
	config->sync();
}

#include "mainwindow.moc"
