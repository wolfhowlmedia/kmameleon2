/***************************************************************************
                          kmmdebug.cc  -  description
                             -------------------
    copyright            : (C) 2002 by Whitehawk Stormchaser
    email                : zerokode@gmx.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#include "kmmdebug.h"

DevDebug::DevDebug( QWidget *parent, const char *name, WFlags f )
    : DlgDebug( parent, name, f )
{
}

DevDebug::~DevDebug()
{
}

void DevDebug::closeEvent(QCloseEvent *)
{

}

#include "kmmdebug.moc"
