/***************************************************************************
                          kmmscreenshots.h  -  description
                             -------------------
    copyright            : (C) 2002 by Whitehawk Stormchaser
    email                : zerokode@gmx.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef _KMMSCREENSHOTS_H__
#define _KMMSCREENSHOTS_H__

//Qt stuff...
#include <qevent.h>

#include "dlgscreenshots.h"

class ScreenShotWindow : public DlgScreenShots
{
Q_OBJECT
public:
	ScreenShotWindow( QWidget *parent=0, const char *name=0, WFlags f=0 );
	~ScreenShotWindow();

protected slots:
	void closeEvent(QCloseEvent *);
};

#endif
