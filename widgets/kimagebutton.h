/***************************************************************************
                          kimagebutton.h  -  description
                             -------------------
    copyright            : (C) 2002 by Whitehawk Stormchaser
    email                : zerokode@gmx.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __KIMAGEBUTTON_H__
#define __KIMAGEBUTTON_H__

#include <qlabel.h>
#include <qevent.h>
#include <qpixmap.h>
#include <qwidget.h>
#include <qframe.h>
#include <qimage.h>

#include <kdebug.h>
class KImageButton : public QLabel
{
Q_OBJECT
public:
	KImageButton(QWidget *parent, QString pixmap, int width = 0, int height = 0); //constructor with image on it
	KImageButton(QWidget *parent, QString pixmap_over, QString pixmap_out, int width = 0, int height = 0); //constructor with press / release image
	bool hasIgnoreTransparency(); //will return, weither it ignores transparent pixels or not
	void setIgnoreTransparency(bool trans); //will set the ignore of transparency

	~KImageButton();


private:
	QPixmap m_pixmapButton, m_pixmapOver, m_pixmapOut;
	QImage m_tempImage;
	bool is_overButton;
	bool is_ignoreTransparency;

protected:
	void mousePressEvent( QMouseEvent *ev );
	void mouseReleaseEvent( QMouseEvent *ev );
	void enterEvent( QEvent *);
	void leaveEvent( QEvent *);

signals:
	void clicked();
	void pressed();
	void released();

	void leaveEvent();
	void enterEvent();
};

#endif // __KIMAGEBUTTON_H__
