/***************************************************************************
                          kimagebutton.cc  -  description
                             -------------------
    copyright            : (C) 2002 by Whitehawk Stormchaser
    email                : zerokode@gmx.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 #include <kimagebutton.h>

KImageButton::KImageButton(QWidget *parent, QString pixmap, int width, int height) : QLabel(parent)
{
	m_pixmapButton = QPixmap (pixmap);
	setPixmap(m_pixmapButton);

	setBackgroundOrigin( QWidget::WindowOrigin );
	is_overButton = false;
	is_ignoreTransparency = false;

	if (width > 0 && height > 0)
		m_pixmapButton.resize(width, height);
}

KImageButton::KImageButton(QWidget *parent, QString pixmap_over, QString pixmap_out, int width, int height) : QLabel(parent)
{
	m_pixmapOver = QPixmap (pixmap_over);
	m_pixmapOut = QPixmap (pixmap_out);

	m_pixmapButton = QPixmap (pixmap_out);
	setPixmap(m_pixmapButton);

	setBackgroundOrigin( QWidget::WindowOrigin );
	is_overButton = true;

	if (width > 0 && height > 0)
	{
		m_pixmapButton.resize(width, height);
		m_pixmapOver.resize(width, height);
		m_pixmapOut.resize(width, height);
	}

	is_ignoreTransparency = false;
}

KImageButton::~KImageButton()
{

}

void KImageButton::mousePressEvent(QMouseEvent * ev)
{
	m_tempImage = m_pixmapButton.convertToImage();
	if (is_ignoreTransparency)
	{
		if (m_tempImage.pixel(ev->x(), ev->y()) != 0)
		{
			kdDebug() << "pressed()" << endl;
			emit pressed();
		}
	}
	else
	{
		kdDebug() << "pressed()" << endl;
		emit pressed();
	}
}

void KImageButton::mouseReleaseEvent(QMouseEvent * ev)
{
	m_tempImage = m_pixmapButton.convertToImage();
	if (rect().contains( ev->pos() ))
	{
		if (is_ignoreTransparency)
		{
			if (m_tempImage.pixel(ev->x(), ev->y()) != 0)
			{
				kdDebug() << "clicked()" << endl;
				emit clicked();
			}
		}
		else
		{
			kdDebug() << "clicked()" << endl;
			emit clicked();
		}
	}

	kdDebug() << "released()" << endl;
	emit released();
}

void KImageButton::enterEvent(QEvent * ev)
{
	if (is_overButton)
	{
		kdDebug() << "enterEvent()" << endl;
		m_pixmapButton = m_pixmapOver;
	}

	m_tempImage = m_pixmapButton.convertToImage();
	setPixmap(m_pixmapButton);

	emit enterEvent();
}

void KImageButton::leaveEvent(QEvent * ev)
{
	if (is_overButton)
	{
		kdDebug() << "enterEvent()" << endl;
		m_pixmapButton = m_pixmapOut;
	}

	m_tempImage = m_pixmapButton.convertToImage();
	setPixmap(m_pixmapButton);

	emit leaveEvent();
}

bool KImageButton::hasIgnoreTransparency()
{
	return is_ignoreTransparency;
}

void KImageButton::setIgnoreTransparency(bool trans)
{
	is_ignoreTransparency = trans;
}

#include <kimagebutton.moc>
