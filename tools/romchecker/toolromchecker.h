/***************************************************************************
                          toolromchecker.h  -  description
                             -------------------
    copyright            : (C) 2002 by Whitehawk Stormchaser
    email                : zerokode@gmx.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef _TOOLROMCHECKER_H__
#define _TOOLROMCHECKER_H__

//KDE
#include <kdebug.h>
#include <kconfig.h>
#include <kapplication.h>
#include <klocale.h>
#include <kstandarddirs.h>
#include <kprocess.h>
#include <kmessagebox.h>
#include <kdeversion.h>

//Qt...
#include <qprogressbar.h>
#include <qpushbutton.h>
#include <qcombobox.h>
#include <qfileinfo.h>
#include <qdir.h>
#include <qstringlist.h>
#include <qlcdnumber.h>
#include <qlabel.h>
#include <qtextedit.h>

//custom...
#include "toolromcheckeriface.h"

class ToolRomCheckerPlugin : public ToolRomChecker
{
Q_OBJECT
public:
	ToolRomCheckerPlugin();
	~ToolRomCheckerPlugin();

private:
	KConfig *config;
	QStringList gamesList; 
	
#if KDE_VERSION >= 306
	KProcess infoProc;
	KProcess vProc;
	KProcess vErrProc;
#else
	KShellProcess infoProc;
	KShellProcess vProc;
	KShellProcess vErrProc;
#endif
	
	unsigned int gameCount;
	bool isCorrect;
	QString conf_xmameBinPath;
	QString options;

private slots:
	void slotCheckRoms();
	void slotReloadRoms();
	void getInfoOutput( KProcess *, char *, int);
	void getOutput( KProcess *, char *, int);
	QString modes(int);
};

#endif
