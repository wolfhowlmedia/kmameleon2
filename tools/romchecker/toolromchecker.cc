/***************************************************************************
                          toolromchecker.cc  -  description
                             -------------------
    copyright            : (C) 2002 by Whitehawk Stormchaser
    email                : zerokode@gmx.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#include "toolromchecker.h"

ToolRomCheckerPlugin::ToolRomCheckerPlugin()
{
//main configuration...
	config = kapp->config();
	statusPixmap->setPixmap(QPixmap(locate("data","kmameleon/tools/minus.png")));

//we load the roms and count the numbers...
	slotReloadRoms();

//connecting...
	connect(btnClose, SIGNAL(clicked()), this, SLOT(close()));
	connect(btnVerify, SIGNAL(clicked()), this, SLOT(slotCheckRoms()));
	connect(&infoProc, SIGNAL( receivedStdout(KProcess *, char *, int)), this, SLOT( getInfoOutput( KProcess *, char *, int)) );
	connect(&vProc, SIGNAL( receivedStdout(KProcess *, char *, int)), this, SLOT( getOutput( KProcess *, char *, int)) );
	connect(&vErrProc, SIGNAL( receivedStdout(KProcess *, char *, int)), this, SLOT( getOutput( KProcess *, char *, int)) );
	connect(btnReload, SIGNAL( clicked() ), this, SLOT( slotReloadRoms() ) );
}

ToolRomCheckerPlugin::~ToolRomCheckerPlugin()
{

}

void ToolRomCheckerPlugin::slotCheckRoms()
{
//Cleanup!!!
	romOutput->clear();
	isCorrect = false;

//OK... NEEDED: Dunamical detection of xmame :)
	if (! modes(0).isEmpty())
		conf_xmameBinPath = modes(0);
	else if (! modes(1).isEmpty())
		conf_xmameBinPath = modes(1);
	else if (! modes(2).isEmpty())
		conf_xmameBinPath = modes(2);
	else if (! modes(3).isEmpty())
		conf_xmameBinPath = modes(3);

	config->setGroup("general");
	QString mameRomPath = config->readEntry("xmame_rom","");
	options ="";
	options += " -rp ";
	options += mameRomPath;

	if (! romStack->currentText().isEmpty()) //will just pop a message if select is empty...
	{
		infoProc.clearArguments();
		#if KDE_VERSION >= 306
			infoProc.setUseShell(true);
		#endif
		infoProc << conf_xmameBinPath << " " << options.local8Bit() << " -lg "  << romStack->currentText() ;
		if (! infoProc.start(KProcess::NotifyOnExit, KProcess::AllOutput))
		{
			kdDebug() <<"Can't exec the process..." << endl;
		}

		vProc.clearArguments();
		#if KDE_VERSION >= 306
			vProc.setUseShell(true);
		#endif
		vProc  << conf_xmameBinPath << " " << options.local8Bit() << " -vr " << romStack->currentText() << " | grep romset";
		if (! vProc.start(KProcess::NotifyOnExit, KProcess::AllOutput))
		{
			kdDebug() <<"Can't exec the process..." << endl;
		}
	}
	else
		KMessageBox::sorry(this,i18n("No roms are selected for checking..."));
}

void ToolRomCheckerPlugin::slotReloadRoms()
{
	gamesList.clear();
	romOutput->clear();
	romStack->clear();
	statusPixmap->setPixmap(QPixmap(locate("data","kmameleon/tools/minus.png")));
	statusLabel->setText(i18n("Please press \"verify\" to check rom..."));

	config->setGroup("general");
	QString mameRomPath = config->readEntry("xmame_rom","");

	if (! mameRomPath.isEmpty())
	{
		QDir d(mameRomPath);
		d.setFilter( QDir::Dirs);

		for ( unsigned int i=0; i<d.count(); i++ )
		{
			if ((d[i] != ".") && (d[i] != ".."))
			{
				gamesList += d[i];
				kdDebug() << "Romchecker: Adding game name [directory]: " << d[i] << endl;
			}
		}

		d.setFilter( QDir::Files);
		QStringList zipGamesList = d.entryList("*.zip");
		for (unsigned int i=0; i<zipGamesList.count(); i++)
		{
				QFileInfo fi(zipGamesList[i]);
				if (gamesList.findIndex(fi.baseName()) == -1)
				{
					//kdDebug() << "Romchecker: Adding game name [zip]: " << fi.baseName() << endl;
					gamesList += fi.baseName();
				}
				else
				{
					kdDebug() << "Romchecker: Ignoring game name [zip]: " << fi.baseName() << endl;
				}
		}

		QStringList bZipGamesList = d.entryList("*.tar.bz2");
		for (unsigned int i=0; i<bZipGamesList.count(); i++)
		{
			QFileInfo fi(bZipGamesList[i]);
			if (gamesList.findIndex(fi.baseName()) == -1)
			{
				kdDebug() << "Romchecker: Adding game name [bZip2]: " << fi.baseName() << endl;
				gamesList += fi.baseName();
			}
			else
			{
				kdDebug() << "Romchecker: Ignoring game name [bZip2]: " << fi.baseName() << endl;
			}
		}
	}
	gamesList.sort();
	romStack->insertStringList(gamesList);
	romCounter->display(romStack->count());
}

void ToolRomCheckerPlugin::getOutput(KProcess *, char *text, int len)
{
	QString temp;
	temp.fill(' ',len+1);
	temp.replace(0,len,text);
	temp[len]='\0';

	QString side1, side2;

//rather write output in the debug window as the kdDebug()
	kdDebug() << temp.local8Bit() << endl;
	kdDebug() << QString::compare(temp.local8Bit().stripWhiteSpace(), QString("romset "+romStack->currentText()+" correct").stripWhiteSpace()) << endl;
	side1 = temp.local8Bit().stripWhiteSpace();
	side2 = QString("romset "+romStack->currentText()+" correct").local8Bit().stripWhiteSpace();
	kdDebug() << endl;
	kdDebug() << "Comparing: \n" << side1 << " | " << side2 << endl;

	if (QString::compare(side1, side2) == 0)
	{
		statusPixmap->setPixmap(QPixmap(locate("data","kmameleon/tools/ok.png")));
		statusLabel->setText(i18n("romset %1 is valid").arg(romStack->currentText()));
		isCorrect = true;
	}
	else
	{
		if(! isCorrect)
		{
			statusPixmap->setPixmap(QPixmap(locate("data","kmameleon/tools/not.png")));
			statusLabel->setText(i18n("romset %1 is invalid").arg(romStack->currentText()));

		//throwing an error output ONLY when needed...
			vErrProc.clearArguments();
			#if KDE_VERSION >= 306
				vErrProc.setUseShell(true);
			#endif
			vErrProc  << conf_xmameBinPath << " " << options.local8Bit() << " -vr " << romStack->currentText();
			if (! vErrProc.start(KProcess::NotifyOnExit, KProcess::AllOutput))
				kdDebug() <<"Can't exec the process..." << endl;
		}
	}
	romOutput->append( temp.local8Bit() );
}

void ToolRomCheckerPlugin::getInfoOutput(KProcess *, char *text, int len)
{
	QString temp;
	temp.fill(' ',len+1);
	temp.replace(0,len,text);
	temp[len]='\0';
	romOutput->append( temp.local8Bit() );
}

QString ToolRomCheckerPlugin::modes(int mode)
{
//we just need to detect one binary, that it will check the roms...
	if (mode == 0)
		conf_xmameBinPath=config->readEntry("xmame_sdl","");
	else if(mode == 1)
		conf_xmameBinPath=config->readEntry("xmame_x11","");
	else if(mode == 2)
		conf_xmameBinPath=config->readEntry("xmame_gl","");
	else if(mode == 3)
		conf_xmameBinPath=config->readEntry("xmame_svga","");

	return conf_xmameBinPath;
}
#include "toolromchecker.moc"
