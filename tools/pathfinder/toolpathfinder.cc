/***************************************************************************
                          toolpathfinder.cc  -  description
                             -------------------
    copyright            : (C) 2002 by Whitehawk Stormchaser
    email                : zerokode@gmx.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#include "toolpathfinder.h"

ToolPathFinderPlugin::ToolPathFinderPlugin()
{
//general
	config=kapp->config();

	findMenu= new KPopupMenu(this);
	findMenu->insertItem( i18n("Show default paths"), this, SLOT(slotFindPaths()) );
	findMenu->insertItem( i18n("Show && set default paths"), this, SLOT(slotFindAndSetPaths()) );

//find dropdown...
	btnFind->setPopup(findMenu);

//connections...
	connect(btnClose, SIGNAL(clicked()), this, SLOT(close()) );
	connect(&pProc, SIGNAL( receivedStdout(KProcess *, char *, int)), this, SLOT( getOutput( KProcess *, char *, int)) );

	setPaths = false;
}

ToolPathFinderPlugin::~ToolPathFinderPlugin()
{

}

void ToolPathFinderPlugin::slotFindPaths()
{
	setPaths = false;
	romOutput -> clear();
	QString mameType = slotMameType();
	if (! mameType.isEmpty())
	{
		QString options = mameType;
		options +=" -sc ";
		options +=" | grep -i / ";

		pProc.clearArguments();
		#if KDE_VERSION >= 306
			pProc.setUseShell(true);
		#endif

		pProc << options.local8Bit();
		if (pProc.start(KProcess::NotifyOnExit, KProcess::AllOutput) == false)
		{
			kdDebug() << "something wrong" << endl;
		}
	}
	else
		KMessageBox::information(this,i18n("This XMame type does not exists or it is not configured"));
}

void ToolPathFinderPlugin::getOutput(KProcess *, char *text, int len)
{
	QStringList list;
	QString temp;
	int pos;
	QString dataStream;

	romOutput -> clear();

	temp.fill(' ',len+1);
	temp.replace(0,len,text);
	temp[len]='\0';
	paths=QStringList::split("\n", temp.local8Bit());
	pathList = temp.local8Bit();

	for ( unsigned int i= 0; i<paths.count(); i++)
	{
		if (paths[i].contains("rom") > 0 && paths[i].contains("/") > 0)
		{
			kdDebug() << paths[i] << endl;
			list += paths[i];
		}
		if (paths[i].contains("samples") > 0 && paths[i].contains("/") > 0)
		{
			kdDebug() << paths[i] << endl;
			list += paths[i];
		}
		if (paths[i].contains("artwork") > 0 && paths[i].contains("/") > 0)
		{
			kdDebug() << paths[i] << endl;
			list += paths[i];
		}
	}

	if (setPaths)
	{
		for ( unsigned int i= 0; i<list.count(); i++)
			kdDebug() << paths[i] << endl;

	//this will handle the paths...
		config->setGroup("general");
		for (int pthEnv =0; pthEnv <=2; pthEnv++)
		{
			switch(pthEnv)
			{
				case 0: //rompath
					pos = list[pthEnv].find("/");
					dataStream = list[pthEnv].mid(pos);
					kdDebug() << dataStream << endl;
					if (config->readEntry("xmame_rom","").isEmpty())
						config->writeEntry("xmame_rom",dataStream);
					else
						if (KMessageBox::questionYesNo(this,i18n("This will override your current rom path settings... Want me to proceed?")) == 3)
							config->writeEntry("xmame_rom",dataStream);
				break;
				case 1: //samplepath
					pos = list[pthEnv].find("/");
					dataStream = list[pthEnv].mid(pos);
					kdDebug() << dataStream << endl;
					if (config->readEntry("xmame_samples","").isEmpty())
						config->writeEntry("xmame_samples",dataStream);
					else
						if (KMessageBox::questionYesNo(this,i18n("This will override your current sample path settings... Want me to proceed?")) == 3)
							config->writeEntry("xmame_samples",dataStream);
				break;
				case 2:
					pos = list[pthEnv].find("/");
					dataStream = list[pthEnv].mid(pos);
					kdDebug() << dataStream << endl;
					if (config->readEntry("xmame_art","").isEmpty())
						config->writeEntry("xmame_art",dataStream);
					else
						if (KMessageBox::questionYesNo(this,i18n("This will override your current artwork path settings... Want me to proceed?")) == 3)
							config->writeEntry("xmame_art",dataStream);
				break;
			} //end switch
		} // end for loop
	} //end if
	romOutput->setText( list.join("\n") );
	list = 0L;
}

void ToolPathFinderPlugin::slotFindAndSetPaths()
{
	romOutput -> clear();
	QString mameType = slotMameType();
	if (! mameType.isEmpty() )
	{
		setPaths = true;
		QString options = mameType;
		options +=" -sc ";
		options +=" | grep -i / ";

		pProc.clearArguments();
		#if KDE_VERSION >= 306
			pProc.setUseShell(true);
		#endif

		pProc << options.local8Bit();
		if (! pProc.start(KProcess::NotifyOnExit, KProcess::AllOutput))
		{
			kdDebug() << "something wrong" << endl;
		}
		kdDebug() << options << endl;
	}
	else
		KMessageBox::information(this,i18n("This XMame type does not exists or it is not configured"));
}

QString ToolPathFinderPlugin::slotMameType()
{
	QString execBin;
	config->setGroup("general");
	switch (mameTypes->currentItem())
	{
		case 0: //SDL
			execBin = config->readEntry("xmame_sdl","");
		break;

		case 1: //X11
			execBin = config->readEntry("xmame_x11","");
		break;

		case 2: //XGL
			execBin = config->readEntry("xmame_gl","");
		break;

		case 3: //SVGALIB
			execBin = config->readEntry("xmame_svgalib","");
		break;
	}

	return execBin;
}
#include "toolpathfinder.moc"
