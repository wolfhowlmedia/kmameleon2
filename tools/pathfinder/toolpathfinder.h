/***************************************************************************
                          toolpathfinder.h  -  description
                             -------------------
    copyright            : (C) 2002 by Whitehawk Stormchaser
    email                : zerokode@gmx.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef _TOOLPATHFINDER_H__
#define _TOOLPATHFINDER_H__

//KDE
#include <kdebug.h>
#include <kprocess.h>
#include <kpopupmenu.h>
#include <klocale.h>
#include <kdeversion.h>
#include <kconfig.h>
#include <kapplication.h>
#include <kmessagebox.h>
#include <kstandarddirs.h>


//Qt
#include <qpushbutton.h>
#include <qtextedit.h>
#include <qcombobox.h>
#include <qstringlist.h>

//custom...
#include "toolpathfinderiface.h"

class ToolPathFinderPlugin : public ToolPathFinder
{
Q_OBJECT
public:
	ToolPathFinderPlugin();
	~ToolPathFinderPlugin();

private:
#if KDE_VERSION >= 306
	KProcess pProc;
#else
	KShellProcess pProc;
#endif

	KPopupMenu *findMenu;
	QString pathList;
	KConfig *config;
	QStringList paths;
	bool setPaths;

private slots:
	void slotFindPaths();
	void slotFindAndSetPaths();
	void getOutput( KProcess *, char *, int);
	QString slotMameType();
};

#endif

